import UIKit

//let name = "John"


//Uhvati memoriju telefona i rinejmuje kada aplikacija krene da ranuje, user defaults se nalazi u telefonu

//Dodavanje
UserDefaults.standard.setValue(name, forKey: "name")

//Brisanje
UserDefaults.standard.removeObject(forKey: "name")

//Izmeniti
UserDefaults.standard.set("Athur", forKey: "name")


//Ako hocemo da dobijemo podatke

let name = UserDefaults.standard.string(forKey: "name") ?? "Unknown user"
print(name)


//cuvanje integera
let age = UserDefaults.standard.integer(forKey: "age") ?? 0
print(age)
