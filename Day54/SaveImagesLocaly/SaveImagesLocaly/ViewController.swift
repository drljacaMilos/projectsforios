//
//  ViewController.swift
//  SaveImagesLocaly
//
//  Created by milos.drljaca on 9.7.21..
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet var imageToSave: UIImageView! {
        didSet{
            imageToSave.image = UIImage(named: "slika21")
        }
    }
    
    @IBOutlet var saveImage: UIButton! {
        didSet{
            saveImage.addTarget(self, action: #selector(ViewController.save), for: .touchUpInside)
        }
    }
    
    @objc func save() {
        if let oneImage = UIImage(named: "slika21") {
            DispatchQueue.global(qos: .background).async {
                self.store(image: oneImage, forKey: "aaa", withStorageType: .fileSystem)
            }
        }
    }
    
    @IBOutlet var savedImage: UIImageView!
    @IBOutlet var displaySavedImage: UIButton! {
        didSet{
            print("Usao")
            displaySavedImage.addTarget(self, action: #selector(ViewController.display), for: .touchUpInside)
        }
    }
    
    @objc func display() {
        DispatchQueue.global(qos: .background).async{
            if let savedImage =  self.retriveImage(forKey: "aaa", inStorageType: .fileSystem){
                DispatchQueue.main.async {
                    self.savedImage.image = savedImage
                }
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    //dva nacina za cuvanje slika. User defaults, File system (File manager) cuvanje na disku
    
    
    enum StorageType {
        case userDefaults
        case fileSystem
    }
    
    private func store(image: UIImage, forKey key: String, withStorageType storageType: StorageType) {
        
        if let pngRepresentation = image.pngData() {
            switch storageType {
            case.fileSystem:
            //save to disk
                if let filePath = filePath(forKey: key){
                do {
                    try pngRepresentation.write(to: filePath, options: .atomic)
                }catch let error {
                    print("Saving file resulted in error ", error)
                }
            }
            case.userDefaults:
                //save to user defaults
                UserDefaults.standard.setValue(pngRepresentation, forKey: key)
            }
        }
    }
    
    private func retriveImage(forKey key: String, inStorageType storageType: StorageType) -> UIImage? {
        switch storageType {
        case.fileSystem: 
        //retrive image from disk
            if let filePath = self.filePath(forKey: key),
               let fileData = FileManager.default.contents(atPath: filePath.path),
            let image =  UIImage(data: fileData) {
                return image
            }
        case.userDefaults:
            //retrive image from user defaults
            if let imageData = UserDefaults.standard.object(forKey: key) as? Data,
               let image = UIImage(data: imageData){
                return image
            }
        }
        return nil
    }
    
//    let userDefault = UserDefaults.standard

    private func filePath (forKey key: String) -> URL? {
        let fileManager = FileManager.default
        
        guard let documentURL = fileManager.urls(for: .documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first else {return nil}
        
        return documentURL.appendingPathComponent(key + ".png")
    }
}
