//
//  ViewController.swift
//  DownloadPhoto
//
//  Created by milos.drljaca on 6/9/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func buttonFunkcija(_ sender: Any) {
        prapereImage()
    }
    @IBOutlet var imageField: UIImageView!
    @IBOutlet var textField: UITextField!
    
    func prapereImage() {
        if let url = URL(string: textField.text!){
            URLSession.shared.dataTask(with: url){
                (data, response, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        self.imageField.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
    }
}

