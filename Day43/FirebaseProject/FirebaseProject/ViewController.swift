//
//  ViewController.swift
//  FirebaseProject
//
//  Created by milos.drljaca on 6/24/21.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let ref = Database.database().reference()
        
        
//        //Change name of existing
//        ref.child("SomeId/name").setValue("Teodos Mukotrpni")
//
//        //Create new database element
//        ref.childByAutoId().setValue(["name" : "Simeon", "age" : 43, "role" : "manager"] )
//
        
        
        //Reading data
        ref.child("SomeId/name").observeSingleEvent(of: .value, with: <#T##(DataSnapshot) -> Void#>)
    }


}

