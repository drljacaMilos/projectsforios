//
//  ViewController.swift
//  Task11
//
//  Created by milos.drljaca on 5/6/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var prikazSlike: UIImageView!
    
    
    @IBOutlet weak var kordinataX: UITextField!
    @IBOutlet weak var kordinataY: UITextField!
    
    @IBOutlet weak var prikazGreske: UILabel!
    
    
    @IBAction func promeni(_ sender: Any) {
        promeniKordinate()
    }
    
    func promeniKordinate() {
        
        prikazGreske.text = ""
        let vrednostX = Int(kordinataX.text!)
        let vrednostY = Int(kordinataY.text!)

        if vrednostX != nil && vrednostY != nil{
            prikazSlike.frame.origin.x = CGFloat(vrednostX!)
            prikazSlike.frame.origin.y = CGFloat(vrednostY!)
        }
        else {
            prikazGreske.text = "Kordinate moraju biti brojevi."
        }
    }

}

