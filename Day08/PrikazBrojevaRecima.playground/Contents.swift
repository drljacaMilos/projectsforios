import UIKit

var brojevi = [
    0:"Nula",
    1:"Jedan",
    2:"Dva",
    3:"Tri",
    4:"Cetri",
    5:"Pet",
    6:"Sest",
    7:"Sedam",
    8:"Osam",
    9:"Devet",
    10:"Deset",
    11:"Jedanaest",
    12:"Dvanaest",
    13:"Trineast",
    14:"Cetrnaest",
    15:"Petnaest",
    16:"Sesnaest",
    17:"Sedamnaest",
    18:"Osamnaest",
    19:"Devetnaest",
    20:"Dvadeset",
    30:"Trideset",
    40:"Cetrdeset",
    50:"Pedeset",
    60:"Sezdeset",
    70:"Sedamdeset",
    80:"Osamdeset",
    90:"Devedeset",
    100:"Sto",
    200:"Dvesta",
    300:"Trista",
    400:"Cetristo",
    500:"Petsto",
    600:"Sesto",
    700:"Sedamsto",
    800:"Osamsto",
    900:"Devetsto"
]

// 1. Data je celobrojna vrednost veca ili jednaka od nule a manja ili jednaka od 9. Ispisati recima tu cifru.
func prikaziBroj(_ broj: Int) -> String {
    //print("Zadatak broj 1: Tekstualna vrednost broja \(broj), je : \(brojevi[broj]!)")
    
    return "\(brojevi[broj]!)"
}


//2. Data je celobrojna vrednost veca ili jednaka od nule a manja od 99. Ispisati recima tu cifru.
func prikaziBrojDoSto(_ broj : Int) -> String {
    
    var konacanBroj = ""
    
    for n in brojevi{
        
        let desetice = (broj / 10) * 10
        let jedinice = broj % 10
        
        let rezultat = desetice + jedinice
        
        var dodatniBrojevi = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
        
        if dodatniBrojevi.contains(rezultat) {
            konacanBroj = String (brojevi[rezultat]!)
        }
        else
        {
            if jedinice == 0{
                konacanBroj =  String ( brojevi [desetice]!)
            }
            else{
                konacanBroj =  String ( brojevi [desetice]!) + " " + String (brojevi [jedinice]!)
            }
                    
        }

        //print("Zadatak broj 2: Tekstualna vrednost broja \(broj), je : \(konacanBroj)")
        break
        
    }
    return "\(konacanBroj)"
}


// 3. Data je celobrojna vrednost veca ili jednaka od nule a manja od 999. Ispisati recima tu cifru.
func prikaziBrojDoHiljadu(_ broj : Int) -> String{
    
    var konacanBroj = ""
    
    for n in brojevi{
        
       
        
        let stotine = (broj / 100) * 100
        let desetice = (broj  % 100 / 10) * 10
        let jedinice = broj % 10
     
        
        let rezultat =  desetice + jedinice
        
        var dodatniBrojevi = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]

        if dodatniBrojevi.contains(rezultat) {
            if stotine != 0 {
                if rezultat == 0{
                    konacanBroj = String(brojevi[stotine]!)
                }else{
                    konacanBroj = String(brojevi[stotine]!) + " " + String (brojevi[rezultat]!)
                }
                
            }else{
                konacanBroj = String (brojevi[rezultat]!)
            }
           
        }
        else
        {
            if stotine != 0 {
                if jedinice == 0{
                    konacanBroj = String ( brojevi [stotine]!) + " " + String ( brojevi [desetice]!)
                }else{
                    konacanBroj = String ( brojevi [stotine]!) + " " + String ( brojevi [desetice]!) + " " + String ( brojevi [jedinice]!)
                }
                
            }
            else{
                if jedinice == 0{
                    konacanBroj = String ( brojevi [desetice]!)
                }
                else{
                    konacanBroj = String ( brojevi [desetice]!) + " " + String (brojevi [jedinice]!)
                }
            }
            
        }
    
        //    print(desetice, jedinice)
       
        //print("Zadatak broj 3: Tekstualna vrednost broja \(broj), je : \(konacanBroj)")
        
        
        break
    }
    return "\(konacanBroj)"
}


// 4. Prikazi broj do 10.000

func prikaziBrojDoDesetHiljada(_ broj : Int) -> String{
    
    var konacanBroj = ""
    
    for n in brojevi {
        
        let ostatak = broj % 1000
        let hiljada = broj / 1000
        
        var dodatniBrojevi = [1, 5, 6, 7, 8, 9]
        
        if hiljada > 0 {
            
            if dodatniBrojevi.contains(hiljada){
                
                var broj = prikaziBroj(hiljada)
                
                if broj == "Jedan"{
                    broj = "Hiljadu"
                }else{
                    broj += " hiljada"
                }
                konacanBroj = broj + " " + prikaziBrojDoHiljadu(ostatak)
            }else
            {
                konacanBroj = prikaziBroj(hiljada) + " " + "hiljade" + " " + prikaziBrojDoHiljadu(ostatak)
            }
            
        }
        else{
            konacanBroj = prikaziBrojDoHiljadu(ostatak)
        }
        
        
        
        //print("Zadatak broj 4: Tekstualna vrednost broja \(broj), je : \(konacanBroj)")
        break
    }
    return "\(konacanBroj)"
    
}


// 5. Prikazi broj do 100.000

func prikaziBrojDoStoHiljada(_ broj : Int){
    
    var konacanBroj = ""

    for n in brojevi {
        
        let ostatak = broj % 1000
        print(ostatak)
        let desetineHiljada = broj / 1000
        print(desetineHiljada)
        var dodatniBrojevi = [1,2,3,4,5,6,7,8,9,10,11,12, 20,21,22, 30,31,32, 40,41,42, 50,51,52, 60,61,62, 70,71,72, 80,81,82, 90,91,92]
        
        
        if desetineHiljada > 0 {
            var brojStotine = prikaziBrojDoDesetHiljada(ostatak)
            
            if brojStotine == "Nula"{
                brojStotine = ""
            }
            
            if dodatniBrojevi.contains(desetineHiljada){
                var brojURecima = prikaziBrojDoSto(desetineHiljada)
                
                
                var noviBrojURecima = ""
                print(brojURecima)
                
                
                if brojURecima.contains(" Jedan"){
                    noviBrojURecima = brojURecima.replacingOccurrences(of: "Jedan", with: "Jedna")
                    
                    konacanBroj = noviBrojURecima + " " + "hiljada" + " " + brojStotine
                }
                else if(brojURecima.contains(" Dva")){
                    noviBrojURecima = brojURecima.replacingOccurrences(of: "Dva", with: "Dve")
                    
                    konacanBroj = noviBrojURecima + " " + "hiljade" + " " + brojStotine
                }
                else{
                    if brojURecima == "Jedan"{
                        brojURecima = "Hiljadu"
                        konacanBroj = brojURecima + " " + brojStotine
                    }else if brojURecima == "Dva"{
                        brojURecima = "Dve"
                        konacanBroj = brojURecima + " " + "hiljade" + " " + brojStotine
                    }
                    else if brojURecima.contains("Tri") || brojURecima.contains("Cetri") {
                        konacanBroj = brojURecima + " " + "hiljade" + " " + brojStotine
                    }
                    else{
                        konacanBroj = prikaziBrojDoSto(desetineHiljada) + " " + "hiljada" + " " + brojStotine
                    }
                    
                }
                
            }
            else
            {
                konacanBroj = prikaziBrojDoSto(desetineHiljada) + " " + "hiljada " + brojStotine
            }
        }

        print("Zadatak broj 5: Tekstualna vrednost broja \(broj), je : \(konacanBroj)")
        break
    }
}


prikaziBroj(4)
prikaziBrojDoSto(12)
prikaziBrojDoHiljadu(123)
prikaziBrojDoDesetHiljada(0)


prikaziBrojDoStoHiljada(94001)
