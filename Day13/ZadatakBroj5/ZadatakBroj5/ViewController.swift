//
//  ViewController.swift
//  ZadatakBroj5
//
//  Created by milos.drljaca on 5/13/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 20
        facebook.layer.cornerRadius = 20
        twitter.layer.cornerRadius = 20
    }


    @IBOutlet weak var twitter: UIButton!
    @IBOutlet weak var facebook: UIButton!
    
    
    @IBOutlet weak var viewNumberOne: UIView!
    @IBOutlet weak var viewNumberTwo: UIView!
    
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    
    @IBOutlet weak var loginButton: UIButton!
}

