//
//  ViewController.swift
//  Circle
//
//  Created by milos.drljaca on 5/13/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        picture.layer.cornerRadius = picture.frame.width / 2
        picture.layer.masksToBounds = true
    }


    @IBOutlet weak var picture: UIImageView!
}

