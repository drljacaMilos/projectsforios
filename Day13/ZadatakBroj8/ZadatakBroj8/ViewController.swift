//
//  ViewController.swift
//  ZadatakBroj8
//
//  Created by milos.drljaca on 5/13/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        shareNow.layer.cornerRadius = 20
        
    }

    @IBOutlet weak var shareNow: UIButton!
    
    @IBOutlet weak var viewNumberOne: UIView!
    
    @IBOutlet weak var viewNumberTwo: UIView!
    
    @IBOutlet weak var viewNumberThree: UIView!
    
    @IBOutlet weak var viewInsideFirstView: UIView!
}

