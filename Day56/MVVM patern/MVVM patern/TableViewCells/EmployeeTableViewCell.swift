//
//  EmployeeTableViewCell.swift
//  MVVM patern
//
//  Created by milos.drljaca on 13.7.21..
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {

    
    @IBOutlet var employeeIdLabel: UILabel!
    @IBOutlet var employeeNameLabel: UILabel!
    
    var employee: EmployeeData? {
        didSet{
            employeeIdLabel.text =  employee?.id as! String
            employeeNameLabel.text = employee?.employeeName
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
