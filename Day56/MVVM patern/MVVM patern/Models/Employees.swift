//
//  Employees.swift
//  MVVM patern
//
//  Created by milos.drljaca on 13.7.21..
//

import Foundation

struct Employees: Codable {
    let status: String
    let data: [EmployeeData]
}

struct EmployeeData: Codable {
    let id: Int
    let employeeName: String
    let employeeSalary: Int
    let employeeAge: Int
    let profileImage: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case employeeName = "employee_name"
        case employeeSalary = "employee_salary"
        case employeeAge = "employee_age"
        case profileImage = "profile_image"
    }
}
