//
//  ViewController.swift
//  MVVM patern
//
//  Created by milos.drljaca on 13.7.21..
//

import UIKit

class ViewController: UIViewController {
    
    //From server we get the names and set it up in table
    
    //Model
    private var employeeViewModel: EmployeesViewModel!
    
    //Table view
    @IBOutlet var employeesTableView: UITableView!
    
    private var dataSource: EmployeeTableViewDataSource<EmployeeTableViewCell, EmployeeData>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callToViewModelForUiUpdate()
    }

    func callToViewModelForUiUpdate() {
        self.employeeViewModel = EmployeesViewModel()
        self.employeeViewModel.bindEmployeeViewModelController = {
        self.updateDataSource()
        }
    }

    func updateDataSource() {
        self.dataSource = EmployeeTableViewDataSource(cellIdentifier: "EmployeeTableViewCell", items: self.employeeViewModel.empData.data, configureCell: { (cell, evm) in
            cell.employeeIdLabel.text = String(evm.id)
            cell.employeeNameLabel.text = evm.employeeName
        })
        
        DispatchQueue.main.async {
            self.employeesTableView.dataSource = self.dataSource
            self.employeesTableView.reloadData()
        }
    }

}

