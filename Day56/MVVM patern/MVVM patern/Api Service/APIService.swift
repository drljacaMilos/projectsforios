//
//  APIService.swift
//  MVVM patern
//
//  Created by milos.drljaca on 13.7.21..
//

import UIKit

class APIService: NSObject {

    private let sourceURL = URL(string: "http://dummy.restapiexample.com/api/v1/employees")!
    
    
    func apiToGetEmployeeData(completion: @escaping (Employees) -> ()){
        URLSession.shared.dataTask(with: sourceURL) {(data, URLResponse, error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                let empData = try! jsonDecoder.decode(Employees.self, from: data)
                completion(empData)
            }
            
        } .resume()
    }
}
