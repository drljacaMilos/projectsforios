//
//  EmployeesViewModel.swift
//  MVVM patern
//
//  Created by milos.drljaca on 13.7.21..
//

import UIKit

class EmployeesViewModel: NSObject {

    private var apiService: APIService!
    
    private(set) var empData: Employees! {
        didSet{
            self.bindEmployeeViewModelController()
        }
    }
    
    
    var bindEmployeeViewModelController: (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService = APIService()
        callFuncToGetEmpData()
    }
    
    func callFuncToGetEmpData() {
        self.apiService.apiToGetEmployeeData { (empData) in
            print(empData)
        }
    }

    
}
