//
//  ViewController.swift
//  ZadatakBroj4
//
//  Created by milos.drljaca on 5/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        

        krug.layer.cornerRadius = 0.5 * krug.bounds.size.width
        
        
        registracija.layer.cornerRadius = 20
        ime.layer.cornerRadius = 20
        prezime.layer.cornerRadius = 20
        email.layer.cornerRadius = 20
        lozinka.layer.cornerRadius = 20
        
    }


    
    @IBOutlet weak var ime: UITextField!
    @IBOutlet weak var prezime: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var lozinka: UITextField!
    
    
    @IBOutlet weak var registracija: UIButton!
    
    @IBOutlet weak var krug: UIImageView!
}

