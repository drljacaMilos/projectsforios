//
//  ViewController.swift
//  ZadatakBroj1
//
//  Created by milos.drljaca on 5/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        signIn.layer.cornerRadius = 25
        signUp.layer.cornerRadius = 25
    }

   
    @IBOutlet weak var signIn: UIButton!
    
    @IBOutlet weak var signUp: UIButton!
    
    @IBOutlet weak var viewNumberOne: UIView!
    
    @IBOutlet weak var viewNumberTwo: UIView!
    
}

