//
//  ViewController.swift
//  ZadatakBroj6
//
//  Created by milos.drljaca on 5/12/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        usernameTextField.layer.borderWidth = 0
        passwordTextField.layer.borderWidth = 0
        
        
        login.layer.cornerRadius = 20
        facebook.layer.cornerRadius = 20
        google.layer.cornerRadius = 20
    }
    
    @IBOutlet weak var viewNumberOne: UIView!
    
    @IBOutlet weak var viewNumberTwo: UIView!
    
    @IBOutlet weak var viewNumberThree: UIView!
    
   
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var facebook: UIButton!
    @IBOutlet weak var google: UIButton!
}

