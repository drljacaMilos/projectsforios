//
//  ViewController.swift
//  ZadatakBroj7
//
//  Created by milos.drljaca on 5/13/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        posalji.layer.cornerRadius = 20
        textNumberOne.layer.cornerRadius = 20
        textNumberTwo.layer.cornerRadius = 20
        textNumberThree.layer.cornerRadius = 20
        textNumberFour.layer.cornerRadius = 20
        textNumberFive.layer.cornerRadius = 20
        
        textNumberOne.layer.masksToBounds = true
        textNumberTwo.layer.masksToBounds = true
        textNumberThree.layer.masksToBounds = true
        textNumberFour.layer.masksToBounds = true
        textNumberFive.layer.masksToBounds = true
    }

    @IBOutlet weak var posalji: UIButton!
    @IBOutlet weak var textNumberOne: UITextField!
    
    @IBOutlet weak var buttonNumberOne: UIButton!



    @IBOutlet weak var textNumberTwo: UITextField!
    

    @IBOutlet weak var textNumberThree: UITextField!
    


    @IBOutlet weak var textNumberFour: UITextField!
    
    @IBOutlet weak var textNumberFive: UITextField!
    

}

