//
//  ViewController.swift
//  HomeworkTwoTables
//
//  Created by milos.drljaca on 5/19/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    var firstData =  [10,20,30,40,50,60,70,80,90,100]
    
    var secondData = ["a", "b", "c", "d","e","f","g","h","i","j"]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == FirstTable{
           return firstData.count
        }else{
            return secondData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == FirstTable{
            
            var cell = FirstTable.dequeueReusableCell(withIdentifier: "myFirstCell") as? MyFirstCell
            
            cell?.textLabel?.text = firstData[indexPath.row].description
            
            return cell!
        }
        else{
            var cell = SecondTable.dequeueReusableCell(withIdentifier: "mySecondCell") as? MySecondCell
            
            cell?.textLabel?.text = secondData[indexPath.row].description
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
            FirstTable.cellForRow(at: indexPath)?.backgroundColor = .red
            
            SecondTable.cellForRow(at: indexPath)?.backgroundColor = .red
    }
    
    
    @IBOutlet weak var FirstTable: UITableView!
    
    @IBOutlet weak var SecondTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

