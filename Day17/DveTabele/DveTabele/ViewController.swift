//
//  ViewController.swift
//  DveTabele
//
//  Created by milos.drljaca on 5/19/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == firstTable{
            return firstData.count
        }else{
            return secondData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == firstTable{
            let cell = firstTable.dequeueReusableCell(withIdentifier: "firstCell") as? MyFirstCell
            
            cell?.textLabel?.text = firstData[indexPath.row].description
            return cell!
        }
        else{
            let cell = secondTable.dequeueReusableCell(withIdentifier: "secondCell") as? MySecondCell
            
            cell?.textLabel?.text = secondData[indexPath.row].description
            
            return cell!
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var firstTable: UITableView!
    
    @IBOutlet weak var secondTable: UITableView!
    
    
    
    let firstData = [1,2,3,4,5,6,7,8,9,10]
    
    let secondData = ["One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten"]
    
}

