//
//  CellForCarDetailTV.swift
//  CarCatalog
//
//  Created by milos.drljaca on 6/11/21.
//

import UIKit

class CellForCarDetailTV: UITableViewCell {

    
    @IBOutlet var descriptionName: UILabel!
    @IBOutlet var descriptionDetail: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
