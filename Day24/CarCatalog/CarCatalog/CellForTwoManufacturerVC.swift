//
//  CellForTwoManufacturerVC.swift
//  CarCatalog
//
//  Created by milos.drljaca on 6/10/21.
//

import UIKit

class CellForTwoManufacturerVC: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var labelView: UILabel!
    
}
