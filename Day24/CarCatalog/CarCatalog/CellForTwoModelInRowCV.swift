//
//  CellForTwoModelInRowCV.swift
//  CarCatalog
//
//  Created by milos.drljaca on 6/10/21.
//

import UIKit

class CellForTwoModelInRowCV: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
}
