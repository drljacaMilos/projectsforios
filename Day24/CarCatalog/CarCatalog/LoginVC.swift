//
//  ViewController.swift
//  CarCatalog
//
//  Created by milos.drljaca on 6/9/21.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        if username == password{
            performSegue(withIdentifier: "loginSegue", sender: self)
        }
        
        DataPreparation.parseDataToModelCar(fileName: "model2", fileExtension: "csv")
        DataPreparation.manufacturerByName()
        
    }
    
    let username: String = ""
    let password: String = ""

    
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var usernameTextField: UITextField!
    
    
    @IBAction func loginAction(_ sender: Any) {
        if let username: String = usernameTextField.text, let password: String = passwordTextField.text {
            if username.count > 2 && password.count > 2 {
                performSegue(withIdentifier: "loginSegue", sender: self)
            }else{
                showAlert("Invalid username or password")
            }
            
        }
        else{
            showAlert("Invalid input min 3 character")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    func showAlert(_ message: String){
        let alert = UIAlertController(title: "Login Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        self.present(alert, animated: true)
    }
}

