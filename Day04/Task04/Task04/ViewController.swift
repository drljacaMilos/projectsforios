//
//  ViewController.swift
//  Task04
//
//  Created by milos.drljaca on 4/27/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var input: UITextField!
    
    @IBOutlet weak var result: UILabel!
    
    
    @IBAction func calculate(_ sender: Any) {
        
        var suma = 0
        
        for c in input.text! {
            
            if let character = c.wholeNumberValue{
                suma = suma + character
            }
            
        }
        
        
        result.text = String(suma)
        
        
    }
    
}

