//
//  ViewController.swift
//  Task05
//
//  Created by milos.drljaca on 4/27/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBOutlet weak var input: UITextField!
    
    @IBOutlet weak var Result: UILabel!
    
    
    @IBAction func calculate(_ sender: Any) {
        
        let stringArray = input.text!.components(separatedBy: CharacterSet.decimalDigits.inverted)

        var sum = 0


        for item in stringArray {
            if let number = Int(item) {
                sum = sum + number
            }
        }
        
        Result.text = String(sum)
    }
}

