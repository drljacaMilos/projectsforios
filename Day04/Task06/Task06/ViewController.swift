//
//  ViewController.swift
//  Task06
//
//  Created by milos.drljaca on 4/27/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBOutlet weak var input: UITextField!
    
    
    @IBAction func Sort(_ sender: Any) {
        
        var arrayNumberOne = ""
        var arrayNumberTwo = ""

        var firstHalf = ""
        var secondHalf = ""

        let halfLength = input.text!.count / 2
        
        firstHalf = String(input.text!.prefix(halfLength))
        secondHalf = String(input.text!.suffix(halfLength))

        for c in firstHalf{
            if c.isNumber{
                arrayNumberOne.append(String(c))
            }
        }

        for c in secondHalf{
            if c.isNumber{
                arrayNumberTwo.append(String(c))
            }
        }

        var arrayNumberOneReversed = String (arrayNumberOne.reversed())
        var arrayNumberTwoReversed = String (arrayNumberTwo.reversed())


        let firstPartCorrected = firstHalf.replacingOccurrences(of: arrayNumberOne, with: arrayNumberOneReversed)

        let secondPartCorrected = secondHalf.replacingOccurrences(of: arrayNumberTwo, with: arrayNumberTwoReversed)

        var correctResult = firstPartCorrected + secondPartCorrected
       
        
        input.text = correctResult

    }
    
    

}

