//
//  Helper.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/21/21.
//

import Foundation
import UIKit

class Helper{
    
    static func makeButtons(_ button: UIButton){
        button.layer.cornerRadius = 15
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    static func tabBarColor(_ tabBarController: UITabBarController){
        tabBarController.tabBar.barTintColor = UIColor.black
        tabBarController.tabBar.tintColor =  UIColor.yellow        
    }
    
    static func hideNavigationBar(_ navigationBar: UINavigationController){
        navigationBar.navigationBar.isHidden = true
    }
    
    static let genreForFilter: [String] = [
        "TV Series",
        "Movie",
        "TV Mini-Series",
        "Prime Video",
        "Drama",
        "Thriller",
        "Action",
        "Adventure",
        "Biography",
        "Comedy",
        "Crime",
        "Drama",
        "Fantasy",
        "History",
        "Horror",
        "Music",
        "Mystery",
        "Reality-TV",
        "Sci-Fi",
        "Thriller",
        "War",
        "Western"
    ]
    
}
