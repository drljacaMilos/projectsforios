//
//  StackViewDisplay.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/24/21.
//

import UIKit

class ButtonForStack: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 15
        layer.borderWidth = 1
        layer.borderColor = CGColor(red: 12, green: 12, blue: 12, alpha: 1)
    }

    
}
