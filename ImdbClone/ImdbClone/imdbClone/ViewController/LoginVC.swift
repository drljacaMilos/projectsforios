//
//  LoginVC.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/28/21.
//

import UIKit
import GoogleSignIn
import Firebase
import FirebaseAuth

class LoginVC: UIViewController, GIDSignInDelegate {
    
    let db =  Firestore.firestore()
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        
        db.collection("Users").whereField("Uid", isEqualTo: String(user.userID)).getDocuments { snapshot, error in
            
            if error == nil && snapshot != nil {
                self.goToProfileViewController()
            }else {
                self.createUser(credential, user.profile.name, user.userID)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = false
        
        GIDSignIn.sharedInstance().delegate = self
    }
    
    @IBOutlet var googleSignInButton: UIButton!
    
    
    @IBAction func googleSignInAction(_ sender: Any) {
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func createUser(_ credential: AuthCredential, _ fullName: String, _ uid: String ){
        
        Auth.auth().signIn(with: credential) { authResult, error in
            if error != nil{
            }else{
                self.db.collection("Users").addDocument(data: ["FullName" : fullName,
                                                               "Uid": uid]){ (error) in
                    if error != nil {
                        print("Error saving user data.")
                    }
                }
                self.goToProfileViewController()
            }
            
        }
    }
    
    func goToProfileViewController(){
        let profileViewController = self.storyboard?.instantiateViewController(identifier: "ProfileVC") as? ProfileVC
        self.view.window?.rootViewController = profileViewController
        self.view.window?.makeKeyAndVisible()
    }

}
