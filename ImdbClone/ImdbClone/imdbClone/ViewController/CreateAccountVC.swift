//
//  CreateAccountVC.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/28/21.
//

import UIKit
import FirebaseAuth
import Firebase

class CreateAccountVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    @IBOutlet var userName: UITextField!
    @IBOutlet var userEmail: UITextField!
    @IBOutlet var userPassword: UITextField!
    @IBOutlet var createAccountButton: UIButton!
    @IBOutlet var buttonShowPassword: CheckedBoxButtons!
    
    
    
    
    @IBAction func createUserAction(_ sender: Any) {
        
        let name = userName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let email = userEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = userPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        Auth.auth().createUser(withEmail: email, password: password) { results, error in
            
            //Check for errors
            if error != nil {
                //There was an error creating the user
                print("Error creating user")
            }else{
                //User was created sucesfully, now store the first and last name
                let db = Firestore.firestore()
                db.collection("Users").addDocument(data: ["FullName" : name, "Uid": results!.user.uid]) { (error) in
                    
                    if error != nil {
                        //Show error message
                        print("Error saving user data.")
                    }
                    
                }
                let profileViewController = self.storyboard?.instantiateViewController(identifier: "ProfileVC") as? ProfileVC
                self.view.window?.rootViewController = profileViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    
    @IBAction func showHidePassword(_ sender: Any) {
        
        if buttonShowPassword.isSelected{
            userPassword.isSecureTextEntry = true
        }else {
            userPassword.isSecureTextEntry = false
        }
        
    }
    
    
    func validateFields() -> String? {
        
        //Check that all fields are filled in
        if userName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            userEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            userPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return "Please fill in all fields"
        }
        
        //Check if the password is secured
        let cleanedPassword = userPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //        if Utilities.isPasswordValid(cleanedPassword) == false {
        //            //Password is not secure enough
        //            return "Please make sure your password is at least 8 charachters, contains a special character and a number."
        //        }
        return nil
    }
    
}

