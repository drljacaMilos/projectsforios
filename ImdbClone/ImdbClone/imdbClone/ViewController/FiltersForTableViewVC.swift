//
//  FiltersForTableViewVC.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/23/21.
//

import UIKit

class FiltersForTableViewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Helper.genreForFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = filtersTableView.dequeueReusableCell(withIdentifier: "cellForFiltersTableViewTV", for: indexPath)
    
        cell.textLabel?.text = Helper.genreForFilter[indexPath.row]
        cell.textLabel?.textColor = .white
        cell.backgroundColor = .darkGray
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filtersTableView.register(CellForFiltersTableViewTV.self, forCellReuseIdentifier: "cellForFiltersTableViewTV")
 
        navigationController?.navigationBar.isHidden = false
    }
    
    
    @IBOutlet var filtersTableView: UITableView!

}
