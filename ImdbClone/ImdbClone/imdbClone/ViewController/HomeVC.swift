//
//  ViewController.swift
//  IMDbProject
//
//  Created by milos.drljaca on 6/15/21.
//

import UIKit

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    
    //Collection views
    @IBOutlet var sliderCollectionView: UICollectionView!
    @IBOutlet var whatToWatchCollectionView: UICollectionView!
    @IBOutlet var topPicksForYouCollectionView: UICollectionView!
    @IBOutlet var fanFavouritesCollectionView: UICollectionView!
    @IBOutlet var exploreWhatStreamingCollectionView: UICollectionView!
    @IBOutlet var exploreMoviesAndTvShowsCollectionView: UICollectionView!
    @IBOutlet var comingSoonCollectionView: UICollectionView!
    @IBOutlet var watchSoonAtHomeCollectionView: UICollectionView!
    
    
    //Table views
    @IBOutlet var boxOfficeTableView: UITableView!
    

    //List of box office
    var listOfBoxOffice: [MovieBoxOffice]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareData()
   
        Helper.tabBarColor(tabBarController!)
        Helper.hideNavigationBar(navigationController!)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func prepareData(){
        //Slider Collection View
        DataPreparation.fetchMovie(url: DataPreparation.sliderUrl){(result) in
            guard let result = result else { return }
            DataPreparation.listOfMoviesInTrending = result
            
            for (i, m) in result.Search.enumerated(){
                let urlForFetch = "http://www.omdbapi.com/?apikey=e709e37&i=\(m.imdbID)"
                if i > 5 { break }
                DataPreparation.fetchMovieBoxOffice(url: urlForFetch) { [self] (movieBoxOffice) in
                    self.listOfBoxOffice?.append(movieBoxOffice!)
                    
                    DispatchQueue.main.async {
                        self.boxOfficeTableView.reloadData()
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.sliderCollectionView.reloadData()
            }
        }
        //What To Watch Collection View
        DataPreparation.fetchMovie(url: DataPreparation.whatToWatchUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfWhatToWatch = result
            
            DispatchQueue.main.async {
                self!.whatToWatchCollectionView.reloadData()
            }
        }
        //Top Picks ForYou Collection View
        DataPreparation.fetchMovie(url: DataPreparation.topPicksForYouUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfTopPicksForYou = result
            
            DispatchQueue.main.async {
                self!.topPicksForYouCollectionView.reloadData()
            }
        }
        //Fan Favourites Collection View
        DataPreparation.fetchMovie(url: DataPreparation.fanFavouritesUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfFanFavourites = result
            
            DispatchQueue.main.async {
                self!.fanFavouritesCollectionView.reloadData()
            }
        }
        //Explore whats streaming Collection View
        DataPreparation.fetchMovie(url: DataPreparation.exploreWhatsStreamingUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfExploreWhatsStreaming = result
            
            DispatchQueue.main.async {
                self!.exploreWhatStreamingCollectionView.reloadData()
            }
        }
        //Explore movies and tvShows Collection View
        DataPreparation.fetchMovie(url: DataPreparation.exploreMoviesAndTvShowsUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfMoviesAndTvShows = result
            
            DispatchQueue.main.async {
                self!.exploreMoviesAndTvShowsCollectionView.reloadData()
            }
        }
        //Coming soon Collection View
        DataPreparation.fetchMovie(url: DataPreparation.comingSoonUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfComingSoon = result
            
            DispatchQueue.main.async {
                self!.comingSoonCollectionView.reloadData()
            }
        }
        //What to watch Collection View
        DataPreparation.fetchMovie(url: DataPreparation.whatToWatchAtHomeUrl){ [weak self] (result) in
            guard let result = result else { return }
            DataPreparation.listOfWatchSoonAtHome = result
            
            DispatchQueue.main.async {
                self!.watchSoonAtHomeCollectionView.reloadData()
            }
        }
        
        
    }
    
    //Number of items in Section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.sliderCollectionView{
            return DataPreparation.listOfMoviesInTrending?.Search.count ?? 0
        }
        if collectionView == self.topPicksForYouCollectionView{
            return DataPreparation.listOfTopPicksForYou?.Search.count ?? 0
        }
        if collectionView == self.whatToWatchCollectionView{
            return DataPreparation.listOfWhatToWatch?.Search.count ?? 0
        }
        if collectionView == self.fanFavouritesCollectionView{
            return DataPreparation.listOfFanFavourites?.Search.count ?? 0
        }
        if collectionView == self.exploreWhatStreamingCollectionView{
            return DataPreparation.listOfExploreWhatsStreaming?.Search.count ?? 0
        }
        if collectionView == self.exploreMoviesAndTvShowsCollectionView{
            return DataPreparation.listOfMoviesAndTvShows?.Search.count ?? 0
        }
        if collectionView == self.comingSoonCollectionView{
            return DataPreparation.listOfComingSoon?.Search.count ?? 0
        }
        if collectionView == self.watchSoonAtHomeCollectionView{
            return DataPreparation.listOfWatchSoonAtHome?.Search.count ?? 0
        }
        return 0
    }
    
    //Cell For Item At
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == sliderCollectionView{
            return prepareSliderCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellForMovieSlider", collectionView: sliderCollectionView, searchMovie: DataPreparation.listOfMoviesInTrending!)
        }else if collectionView == whatToWatchCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellWhatToWatch", collectionView: whatToWatchCollectionView, searchMovie: DataPreparation.listOfWhatToWatch!)
        }else if collectionView == topPicksForYouCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellTopPicksForYou", collectionView: topPicksForYouCollectionView, searchMovie: DataPreparation.listOfTopPicksForYou!)
        }else if collectionView == fanFavouritesCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellFanFavourites", collectionView: fanFavouritesCollectionView, searchMovie: DataPreparation.listOfFanFavourites!)
        }else if collectionView == exploreWhatStreamingCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellExploreWhatIsStreaming", collectionView: exploreWhatStreamingCollectionView, searchMovie: DataPreparation.listOfExploreWhatsStreaming!)
        }else if collectionView == exploreMoviesAndTvShowsCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellExploreMoviesAndTVshows", collectionView: exploreMoviesAndTvShowsCollectionView, searchMovie: DataPreparation.listOfMoviesAndTvShows!)
        }else if collectionView == comingSoonCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellComingSoon", collectionView: comingSoonCollectionView, searchMovie: DataPreparation.listOfComingSoon!)
        }else if collectionView == watchSoonAtHomeCollectionView {
            return prepareCellForCollectionView(indexPath: indexPath, cellIdentifier: "cellWatchSoonAtHome", collectionView: watchSoonAtHomeCollectionView, searchMovie: DataPreparation.listOfWatchSoonAtHome!)
        }
        
        return UICollectionViewCell()
    }
    
    //Size for item at
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == sliderCollectionView{
            return  CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
        return CGSize(width: 100, height: 230)
    }
    
    //Did select Item at
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    //Prepare shared cell for collection view
    func prepareCellForCollectionView( indexPath: IndexPath, cellIdentifier: String, collectionView: UICollectionView, searchMovie: SearchMovies ) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! SharedCellForCV
        
        cell.movieName.text = searchMovie.Search[indexPath.row].title
        cell.movieRating.text = searchMovie.Search[indexPath.row].imdbID
        cell.movieYear.text = searchMovie.Search[indexPath.row].year
        
        if collectionView == exploreWhatStreamingCollectionView{
            cell.watchlistButton.setTitle("Watch now", for: .normal)
        }
        if collectionView == exploreMoviesAndTvShowsCollectionView{
            cell.watchlistButton.setTitle("Screening Times", for: .normal)
        }
        if collectionView == topPicksForYouCollectionView{
            cell.watchlistButton.setTitle("Watchlist", for: .normal)
        }
        
        if collectionView == comingSoonCollectionView || collectionView == watchSoonAtHomeCollectionView{
            cell.watchlistButton.isHidden = true
        }
        
        if let urlString =  searchMovie.Search[indexPath.row].poster as? String,
           let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        cell.movieImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        return cell
    }
    
    //Prepare slider cell for collection view
    func prepareSliderCellForCollectionView (indexPath: IndexPath, cellIdentifier: String, collectionView: UICollectionView, searchMovie: SearchMovies ) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CellForMovieSliderCV
        
        if let urlString =  searchMovie.Search[indexPath.row].poster as? String,
           let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        cell.movieImage.image = UIImage(data: data)
                        cell.smallMovieImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        cell.movieName.text = searchMovie.Search[indexPath.row].title
        cell.shortDescription.text = searchMovie.Search[indexPath.row].type
        return cell
    }
    
    //Table view number of rows in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOfBoxOffice?.count ?? 0
    }
    
    //Table view cell for row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellForTopBoxOffice", for: indexPath) as! CellTopBoxOfficeTV
        
        cell.movieNumber.text = String(indexPath.row + 1)
        cell.movieName.text = listOfBoxOffice?[indexPath.row].title ?? "N/A"
        cell.movieBoxOffice.text = listOfBoxOffice?[indexPath.row].boxOffice ?? "N/A"
        
        return cell
    }
    
    //Height for row cell of Table view
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

