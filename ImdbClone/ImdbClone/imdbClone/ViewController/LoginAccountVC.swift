//
//  LoginAccountVC.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/28/21.
//

import UIKit
import FirebaseAuth

class LoginAccountVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet var userEmail: UITextField!
    @IBOutlet var userPassword: UITextField!
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var buttonShowPassword: CheckedBoxButtons!
    
    
    @IBAction func signInAction(_ sender: Any) {
        
        let email = userEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = userPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        print("Email \(email)")
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if error != nil {
                print("Error in Sign in")
                print(error?.localizedDescription)
            }else{
                let profileViewController = self.storyboard?.instantiateViewController(identifier: "ProfileVC") as? ProfileVC
                self.view.window?.rootViewController = profileViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
        
    }
    
    @IBAction func showHidePassword(_ sender: Any) {
        if buttonShowPassword.isSelected{
            userPassword.isSecureTextEntry = true
        }else{
            userPassword.isSecureTextEntry = false
        }
    }
}
