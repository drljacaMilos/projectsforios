//
//  CellWhatToWatchCV.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/17/21.
//

import UIKit

class CellWhatToWatchCV: UICollectionViewCell {
    
    @IBOutlet var movieImage: UIImageView!
    @IBOutlet var movieRating: UILabel!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var movieYear: UILabel!
    
    @IBOutlet var buttonWatchlist: UIButton!
}
