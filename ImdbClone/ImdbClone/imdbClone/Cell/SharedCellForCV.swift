//
//  CellForTopPicksForYou.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/21/21.
//

import UIKit

class SharedCellForCV: UICollectionViewCell {
    
    @IBOutlet var movieImage: UIImageView!
    @IBOutlet var movieRating: UILabel!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var movieYear: UILabel!
    
    @IBOutlet var watchlistButton: UIButton!
}
