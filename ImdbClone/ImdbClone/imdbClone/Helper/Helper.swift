//
//  Helper.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/20/21.
//

import Foundation

class Helper{
    
   static func makeButtons(_ button: UIButton){
        button.layer.cornerRadius = 15
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
}
