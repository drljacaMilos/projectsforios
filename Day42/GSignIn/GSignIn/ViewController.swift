//
//  ViewController.swift
//  GSignIn
//
//  Created by milos.drljaca on 6/23/21.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController, GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print(user.userID!)
        }
    }
    

    
    @IBOutlet var buttonForSignIn: GIDSignInButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
         // Automatically sign in the user.
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()

    }


    @IBAction func login(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
       
        GIDSignIn.sharedInstance().signIn()

    }
}

