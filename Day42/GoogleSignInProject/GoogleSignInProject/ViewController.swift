//
//  ViewController.swift
//  GoogleSignInProject
//
//  Created by milos.drljaca on 6/23/21.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController, GIDSignInDelegate {
    
    
    
    /*ADDED IMPORT, PROTOCOL GIDSIGNIN DELEGATE
     ADDED TWO BUTTON ACTIONS FOR LOG IN AND LOG OUT, AND ONE LABEL FOR DISPLAYING NAME
     ADDED TWO METHODS DID SIGN IN FOR, DID DISCONNECT WITH
     */
    
    @IBOutlet var googleUserName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().delegate = self
    }
    
    @IBAction func googleLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    @IBAction func googleLogout(_ sender: Any) {
        googleUserName.text = ""
        print("USER ACCESS TOKEN \(GIDSignIn.sharedInstance()?.currentUser.authentication.accessToken)")
        print("USER = \(GIDSignIn.sharedInstance()?.currentUser.profile.familyName)") // true - signed in
        GIDSignIn.sharedInstance()?.signOut()
        print("USER POSLE SIGN OUTA = \(GIDSignIn.sharedInstance()?.currentUser != nil)")
        GIDSignIn.sharedInstance()?.disconnect()
        print("USER POSLE DISCONECTA = \(GIDSignIn.sharedInstance()?.currentUser != nil)")
        
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        
        googleUserName.text = String(user.profile.givenName)
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        // ...
        
        
        print( "Prezime \(GIDSignIn.sharedInstance()?.currentUser.profile.givenName)")
        print(fullName, email)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        //        print("USER ACCESS TOKEN \(GIDSignIn.sharedInstance()?.currentUser.authentication.accessToken)")
        //        print(GIDSignIn.sharedInstance()?.currentUser != nil) // true - signed in
        //        print("POSLE \(GIDSignIn.sharedInstance()?.currentUser.authentication != nil)")
        print("Did Disconnect Metoda USER =  \(GIDSignIn.sharedInstance()?.currentUser != nil)")
        print("User disconectect")
    }
}

