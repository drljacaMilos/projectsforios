import UIKit

var greeting = "Hello, playground"

do{
    //make sure the json is in the format we expect
    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]{
        //try to read out a string array
        if let names = json["names"] as? [String]{
            print(names)
        }
    }
    
}catch let error as NSError{
    print("Failed to load: \(error.localizedDescription)")
}
