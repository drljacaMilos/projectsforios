//
//  ViewController.swift
//  HomeworkImages
//
//  Created by milos.drljaca on 5/23/21.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let reuseIdentifier = "controllCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myCollectionView.dataSource = self
        myCollectionView.delegate   = self
    }
    
    
    struct Networks {
        let logo : String
    }
    
    let model = [
        Networks(logo: "slika1"),
        Networks(logo: "slika2"),
        Networks(logo: "slika3"),
        Networks(logo: "slika4"),
        Networks(logo: "slika5"),
        Networks(logo: "slika6"),
        Networks(logo: "slika7"),
        Networks(logo: "slika8"),
        Networks(logo: "slika9"),
        Networks(logo: "slika10"),
        Networks(logo: "slika11"),
        Networks(logo: "slika12"),
        Networks(logo: "slika13"),
        Networks(logo: "slika14"),
        Networks(logo: "slika15"),
        Networks(logo: "slika16"),
        Networks(logo: "slika17"),
        Networks(logo: "slika18"),
        Networks(logo: "slika19"),
        Networks(logo: "slika20"),
        Networks(logo: "slika21"),
    ]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? MyCollectionViewCell
        
       cell?.slika.image = UIImage (named : model[indexPath.row].logo)
        cell?.backgroundColor = .cyan

       return cell!
    }
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedItem = indexPath.item + 1
        print("You selected: \(selectedItem)")
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = .lightGray
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = .red
    }
}

