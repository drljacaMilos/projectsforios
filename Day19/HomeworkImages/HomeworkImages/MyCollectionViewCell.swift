//
//  MyCollectionViewCell.swift
//  HomeworkImages
//
//  Created by milos.drljaca on 5/23/21.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var slika: UIImageView!
}
