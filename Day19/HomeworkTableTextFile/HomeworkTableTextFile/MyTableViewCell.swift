//
//  MyTableViewCell.swift
//  HomeworkTableTextFile
//
//  Created by milos.drljaca on 5/24/21.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var prosecnaOcena: UILabel!
    @IBOutlet weak var ime: UILabel!
    @IBOutlet weak var prezime: UILabel!
    @IBOutlet weak var mesto: UILabel!
    @IBOutlet weak var godinaRodjenja: UILabel!
    @IBOutlet weak var visina: UILabel!
    
}
