//
//  ViewController.swift
//  HomeworkTableTextFile
//
//  Created by milos.drljaca on 5/24/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as? MyTableViewCell
        
        cell?.ime.text = students[indexPath.row].ime
        cell?.prezime.text = students[indexPath.row].prezime
        cell?.prosecnaOcena.text = String(students[indexPath.row].prosecnaOcena)
        
        cell?.godinaRodjenja.text = String(students[indexPath.row].godinaRodjenja)
        cell?.mesto.text = String(students[indexPath.row].mesto)
        cell?.visina.text = String(students[indexPath.row].visina)
        
        return cell!
        
    }
    
    
    struct Student {
        var ime: String
        var prezime: String
        var mesto: String
        var godinaRodjenja: Int
        var visina: Int
        var prosecnaOcena: Double
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        readTextFromFile()
        //simpleParseText()
        parseToModel()
    }

    //FileName and FileNameExtension
    let fileName = "textFile"
    let fileNameExtension = "csv"
    
    //FileContent and Array of Student
    var fileContent : String?
    var students: [Student] = []
    
    


    //Reading text from file
    func readTextFromFile(){
        let path = Bundle.main.path(forResource: fileName, ofType: fileNameExtension)
        
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        
        if let fileContent = self.fileContent{
            print(fileContent)
        }
    }
    
    //Parsing the text
    func simpleParseText() {
        
        if let fileContent = self.fileContent{
            let lineByLineArray = fileContent.split(separator: "\n")
            print(lineByLineArray)
            
            for row in lineByLineArray{
                print(row)
                let lineToArray = row.split(separator: ",")
                for column in lineToArray{
                    print(column)
                }
            }
        }
    }
    
    //Parse to model
    func parseToModel(){
        
        if let fileContent = self.fileContent{
            let lineByLineArray = fileContent.split(separator: "\n")
            
            students = []
            
            for row in lineByLineArray{
                let lineToArray = row.split(separator: ",")
                
                let ime = String(lineToArray[0])
                let prezime = String(lineToArray[1])
                let mesto = String(lineToArray[2])
                let godinaRodjenja = Int(lineToArray[3]) ?? 0
                let visina = Int(lineToArray[4]) ?? 0
                let prosecnOcena = Double(lineToArray[5]) ?? 0.00
                
                let newStudent = Student(ime: ime, prezime: prezime, mesto: mesto, godinaRodjenja: godinaRodjenja, visina: visina, prosecnaOcena: prosecnOcena)
                students.append(newStudent)
            }
            
        }
        print(students)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

}

