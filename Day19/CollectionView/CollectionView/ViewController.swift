//
//  ViewController.swift
//  CollectionView
//
//  Created by milos.drljaca on 5/21/21.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate,
    UICollectionViewDataSource{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        cell.ispisBroja.text = items[indexPath.row]
        cell.backgroundColor = .cyan
        
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell at \(indexPath.item)")
    }
    
    let reuseIdentifier = "cell"
    var items = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

 
    
}

