//
//  ViewController.swift
//  ParsingTextFile
//
//  Created by milos.drljaca on 5/21/21.
//

import UIKit

class ViewController: UIViewController {

    
    struct Student {
        var ime: String
        var prezime: String
        var mesto: String
        var godinaRodjenja: Int
        var visina: Int
        var prosecnaOcena: Double
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        readTextFromFile()
//        simpleParseText()
        parseToModel()
    }

    let fileName = "textFile"
    let fileNameExtension = "csv"
    
    var fileContent : String?
    var Students: [Student] = []
    
    func readTextFromFile(){
        let path = Bundle.main.path(forResource: fileName, ofType: fileNameExtension)
        
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        if let fileContent = self.fileContent{
            print(fileContent)
        }
    }

    
    func simpleParseText(){
        if let fileContent = self.fileContent {
            let lineByLineArray = fileContent.split(separator: "\n")
            print(lineByLineArray)
            
            for row in lineByLineArray{
                print(row)
                let lineToArray = row.split(separator: ",")
                for column in lineToArray{
                    print(column)
                }

            }
        }
    }
    
    func parseToModel() {
        if let fileContent = self.fileContent {
            let lineByLineArray = fileContent.split(separator: "\n")
            
            Students = []
            
            for row in lineByLineArray{
                let lineToArray = row.split(separator: ",")
                
                let ime = String(lineToArray[0])
                let prezime = String(lineToArray[1])
                let mesto = String(lineToArray[2])
                let godinaRodjenja = Int(lineToArray[3]) ?? 0
                let visina = Int(lineToArray[4]) ?? 0
                let prosecnaOcena = Double(lineToArray[5]) ?? 0.00
                
                
                let newStudent = Student(ime: ime, prezime: prezime, mesto: mesto, godinaRodjenja: godinaRodjenja, visina: visina, prosecnaOcena: prosecnaOcena)
                Students.append(newStudent)
            }
            }
        print(Students)
    }
}

