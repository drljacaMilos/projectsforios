//
//  ViewController.swift
//  MVIApp
//
//  Created by milos.drljaca on 19.7.21..
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
    
    //Cordinates all layers functionality
    
    @IBOutlet var employeeNameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var slotLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    
    private var intent: Intent?
    private let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        intent = Intent()
        
        //binding VC to Store as Single Source of Truth.
        intent?.bind(to: self)
        
        //binding user interaction to Intent
        bindNextButton()
    }
    
    
    public func update(with employee: Event<State>){
        if let value = employee.element {
            employeeNameLabel.text = (value.employee[value.index].name)
            dateLabel.text = (value.employee[value.index].date)
            slotLabel.text = (value.employee[value.index].slot)
        }
    }
    
    func bindNextButton() {
        nextButton.rx.tap.bind{self.intent?.onNext()
        }.disposed(by: disposeBag)
    }
}

