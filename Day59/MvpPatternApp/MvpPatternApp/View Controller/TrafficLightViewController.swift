//
//  ViewController.swift
//  MvpPatternApp
//
//  Created by milos.drljaca on 19.7.21..
//

import UIKit

class TrafficLightViewController: UIViewController, TrafficLightViewDelegate {
    
    
    @IBOutlet var descriptionLabel: UILabel!
    private let trafficLightPresenter = TrafficLightPresenter(trafficLightService: TrafficLightService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        trafficLightPresenter.setViewDelegate(trafficLightViewDelegate: self)
    }

    
    @IBAction func redLightAction(_ sender: Any) {
        trafficLightPresenter.trafficLightColorSelected(colorName:"Red")
    }
    
    @IBAction func yellowLightAction(_ sender: Any) {
        trafficLightPresenter.trafficLightColorSelected(colorName:"Yellow")
    }
    
    @IBAction func greenLightAction(_ sender: Any) {
        trafficLightPresenter.trafficLightColorSelected(colorName:"Green")
    }
    
    
    func displayTrafficLight(description: (String)) {
        descriptionLabel.text = description
    }
}

