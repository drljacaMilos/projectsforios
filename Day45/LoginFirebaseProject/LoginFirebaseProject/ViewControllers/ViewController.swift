//
//  ViewController.swift
//  LoginFirebaseProject
//
//  Created by milos.drljaca on 6/25/21.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    
    
    func setUpElements() {
        
        //Style the elements
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(loginButton)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
    }
    
}

