//
//  LoginViewController.swift
//  LoginFirebaseProject
//
//  Created by milos.drljaca on 6/25/21.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    
    //Form Fields
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    
    //Login button
    @IBOutlet var loginButton: UIButton!
    
    //Error label
    @IBOutlet var errorLabel: UILabel!
    
    //Set elements of storyboard
    func setUpElements(){
        
        //Hide the error label
        errorLabel.alpha = 0
        
        //Style the elements
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        
        Utilities.styleFilledButton(loginButton)
    }
    
    
    
    //Login action button
    @IBAction func loginTapped(_ sender: Any) {
       
        //Create cleaned version
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    
        //Signing in the user
        Auth.auth().signIn(withEmail: email, password: password) { Result, error in
            if error != nil {
                //Could not sign in
                self.errorLabel.text = error!.localizedDescription
                self.errorLabel.alpha = 1
            }else{
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpElements()
    }
}
