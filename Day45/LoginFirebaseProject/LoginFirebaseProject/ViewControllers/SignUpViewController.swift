//
//  SignUpViewController.swift
//  LoginFirebaseProject
//
//  Created by milos.drljaca on 6/25/21.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore


class SignUpViewController: UIViewController {
    
    //Form Fields
    
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    //Sign in button
    @IBOutlet var signUpButton: UIButton!
    
    //Error displayer
    @IBOutlet var errorLabel: UILabel!
    
    //set elements of storyboard
    func setUpElements() {
        
        //Hide the error label
        errorLabel.alpha = 0
        
        //Style the elements
        Utilities.styleTextField(firstNameTextField)
        Utilities.styleTextField(lastNameTextField)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(signUpButton)
    }
    
    
    //Method for validating the fields
    /*
     Check the fields and validate that the data is correct. If everything is correct, this method returns nil. Otherwise, it returns the error message
     */
    func validateFields() -> String? {
        
        //Check that all fields are filled in
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return "Please fill in all fields"
        }
        
        //Check if the password is secured
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilities.isPasswordValid(cleanedPassword) == false {
            //Password is not secure enough
            return "Please make sure your password is at least 8 charachters, contains a special character and a number."
        }
        return nil
    }
    
    //Sign in Action button
    @IBAction func signUpTapped(_ sender: Any) {
        
        //Validate the fields
        let error = validateFields()
        
        if error != nil {
            
            //There is something wrong with the fields, show error message
            errorLabel.text = error!
            errorLabel.alpha = 1
            
        }else{
            //Create cleaned version of the data
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName  = lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            //Create a user
            Auth.auth().createUser(withEmail: email, password: password) { results, error in
                
                //Check for errors
                if error != nil {
                    //There was an error creating the user
                    self.showError("Error creating user")
                }else{
                    //User was created sucesfully, now store the first and last name
                    let db = Firestore.firestore()
                    db.collection("users").addDocument(data: ["firstname" : firstName, "lastname": lastName, "uid": results!.user.uid]) { (error) in
                        
                        if error != nil {
                            //Show error message
                            self.showError("Error saving user data.")
                        }
                        
                    }
                    //Transition to the home screen
                    self.transitionToHome()
                }
            }
        }
    }
    
    
    func showError (_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        
    }
    
    func transitionToHome(){
        let homeViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeViewController) as? HomeViewController
        
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
        
    }
    
    
    
    
    
}
