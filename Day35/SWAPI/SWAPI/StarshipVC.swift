//
//  StarshipVC.swift
//  SWAPI
//
//  Created by milos.drljaca on 6/14/21.
//

import UIKit

class StarshipVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataPreparation.starShipArr.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "starShipCell", for: indexPath) as! StarshipTC
        
        cell.starshipName.text = DataPreparation.starShipArr[indexPath.row].name

        print(DataPreparation.starShipArr[indexPath.row].name)
        return cell
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  
    
    @IBOutlet var tableView: UITableView!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails"{
            let controller = segue.destination as! DetailsVC
            
            controller.selectedShip = DataPreparation.starShipArr[self.tableView.indexPathForSelectedRow!.row]
            
        }
    }
    
    
}
