//
//  DetailsTC.swift
//  SWAPI
//
//  Created by milos.drljaca on 6/14/21.
//

import UIKit

class DetailsTC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet var rightValue: UILabel!
    
    @IBOutlet var leftValue: UILabel!
    
}
