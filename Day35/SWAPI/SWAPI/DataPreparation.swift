//
//  DataPreparation.swift
//  SWAPI
//
//  Created by milos.drljaca on 6/14/21.
//

import Foundation


class DataPreparation {
    static let decoder = JSONDecoder()
    
    static var properties = ["Name", "Model", "Manufacturer","Cost In Credits","Max Atmosphering Speed", "Passengers", "Cargo Capacity","Consumables","Hyperdrive Rating","MGLT","Starship Class","Pilots", "Films",  "Created", "Edited", "Url"]
    
    
    static var starShipArr: [Starship] = []
    
    
    static let url = URL(string: "https://swapi.dev/api/starships/")!
    static var task: URLSessionTask!
    static var photoInfo: FullStarship?
    
    
    static func getData() {
        task = URLSession.shared.dataTask(with: url){[self] (data, response, error) in
            if let data = data, let photoInfo: FullStarship = try? decoder.decode(FullStarship.self, from: data){
                print(photoInfo)
                
                for item in photoInfo.results{
                    DataPreparation.starShipArr.append(item)
                }
            }
            
        }
        task.resume()
    }
    
}
