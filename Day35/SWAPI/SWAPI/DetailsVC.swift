//
//  DetailsVC.swift
//  SWAPI
//
//  Created by milos.drljaca on 6/14/21.
//

import UIKit

class DetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    var selectedShip: Starship? = nil
    
    var shipDetails: [String?] {
        get{
            return [
                selectedShip?.name, selectedShip?.model, selectedShip?.manufacturer, selectedShip?.cost_in_credits, selectedShip?.max_atmosphering_speed, selectedShip?.passengers, selectedShip?.cargo_capacity, selectedShip?.consumables, selectedShip?.hyperdrive_rating, selectedShip?.MGLT, selectedShip?.starship_class, selectedShip?.pilots.first, selectedShip?.films.first, selectedShip?.created, selectedShip?.edited, selectedShip?.url]
        }
        set {
            self.shipDetails = [selectedShip?.name, selectedShip?.model, selectedShip?.manufacturer, selectedShip?.cost_in_credits, selectedShip?.max_atmosphering_speed, selectedShip?.passengers, selectedShip?.cargo_capacity, selectedShip?.consumables, selectedShip?.hyperdrive_rating, selectedShip?.MGLT, selectedShip?.starship_class, selectedShip?.pilots.first, selectedShip?.films.first, selectedShip?.created, selectedShip?.edited, selectedShip?.url]
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = selectedShip?.name
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataPreparation.properties.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath) as! DetailsTC
        
        cell.leftValue.text = DataPreparation.properties[indexPath.row]
        cell.rightValue.text = shipDetails[indexPath.row]
        
        return cell
    }
    
    
    
    

}
