//
//  Starship.swift
//  SWAPI
//
//  Created by milos.drljaca on 6/14/21.
//

import Foundation


struct Starship: Codable {
    let name: String
    let model: String
    let manufacturer: String
    let cost_in_credits: String
    let max_atmosphering_speed: String
    let passengers: String
    let cargo_capacity: String
    let consumables: String
    let hyperdrive_rating: String
    let MGLT: String
    let starship_class: String
    let pilots: [String]
    let films: [String]
    let created: String
    let edited: String
    let url: String
}

struct FullStarship: Codable {
    let count: Int
    let next: String
    let previos: String?
    let results: [Starship]
}
