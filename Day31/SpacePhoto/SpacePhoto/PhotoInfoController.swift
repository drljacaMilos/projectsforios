//
//  PhotoInfoController.swift
//  SpacePhoto
//
//  Created by milos.drljaca on 6/8/21.
//

import Foundation

class PhotoInfoController{
    
    func fetchPhotoInfo(completion: @escaping (PhotoInfo?) -> Void){
        
        let baseUrl = URL(string: "https://api.nasa.gov/planetary/apod")!
        let query: [String : String] = ["api_key" : "DEMO_KEY"]
        
        let url = baseUrl.withQueries(query)!

        let task = URLSession.shared.dataTask(with: url){ (data, response, error) in
            let jsonDecoder = JSONDecoder()
            if let data = data,
        //       let photoDictonary = try? jsonDecoder.decode([String : String].self, from: data){
               let photoInfo = try? jsonDecoder.decode(PhotoInfo.self, from: data){
                
                completion(photoInfo)
                print("Data success.\(photoInfo)")
            }else{
                print("Data was not returned.")
                completion(nil)
            }

        }

        task.resume()
        
    }
    
    
}
