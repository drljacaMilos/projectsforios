//
//  ViewController.swift
//  SpacePhoto
//
//  Created by milos.drljaca on 6/8/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        descriptionLabel.text = ""
        copyrightLabel.text = ""
        
        photoInfoController.fetchPhotoInfo{(photoInfo) in
            if let photoInfo = photoInfo{
                self.updateUI(with: photoInfo)
            }
        }
    }

    let photoInfoController = PhotoInfoController()
    
    

    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var copyrightLabel: UILabel!
    
    @IBOutlet var imageOutlet: UIImageView!
    
    func updateUI(with photoInfo: PhotoInfo){
//        let task = URLSession.shared.dataTask(with: photoInfo.url, completionHandler: { (data, response, error) in
//            guard let data = data,
//                  let image = UIImage(data: data) else{ return }
//            DispatchQueue.main.async {
//
//                self.descriptionLabel.text = photoInfo.description
//                self.imageOutlet.image = image
//
//                print("Photo Description : \(photoInfo.description)")
//                if let copyRight = photoInfo.copyright{
//                    self.copyrightLabel.text = "copyright\(copyRight)"
//                }else{
//                    self.copyrightLabel.isHidden = true
//                }
//            }
//        })
//        task.resume()
        
        guard let url = photoInfo.url.withHTTPS() else{return}
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                guard let data = data,
                                  let image = UIImage(data: data) else{ return }
                            DispatchQueue.main.async {
                
                                self.descriptionLabel.text = photoInfo.description
                                self.imageOutlet.image = image
                
                                print("Photo Description : \(photoInfo.description)")
                                if let copyRight = photoInfo.copyright{
                                    self.copyrightLabel.text = "copyright\(copyRight)"
                                }else{
                                    self.copyrightLabel.isHidden = true
                                }
                            }
                        })
                        task.resume()
    }
}

