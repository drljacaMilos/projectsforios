import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

struct PhotoInfo: Codable {
    var title: String
    var description: String
    var url: URL

    
    enum CodingKeys: String, CodingKey{
        case title = "Naslov"
        case description = "Opis"
        case url
    }
    
    init(from decoder:Decoder) throws{
        let valueContainer = try decoder.container(keyedBy: CodingKeys.self)
        
        self.title = try valueContainer.decode(String.self, forKey: CodingKeys.title)
        self.description = try valueContainer.decode(String.self, forKey: CodingKeys.description)
        self.url = try valueContainer.decode(URL.self, forKey: CodingKeys.url)
    }
    
}

//Staticka metoda primenjiva svugde
extension URL{
    func withQueries(_ queries: [String: String]) -> URL?{
        
        //izvlacenje componenti
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        //mapiranje komponenti
        components!.queryItems = queries.map{
            URLQueryItem(name: $0.0, value: $0.1)
        }
        return components!.url
    }
}


//hendler za rekvestove i klousure
func fetchPhotoInfo(completion: @escaping (PhotoInfo) -> Void){
    print("BLABLA")    //URL
    let baseUrl = URL(string: "https://api.nasa.gov/planetary/apod")!
    //Kveri parametri
    let query: [String : String] = ["api_key" : "DEMO_KEY"]
    
    let url = baseUrl.withQueries(query)!
    
    print(url)
    let task = URLSession.shared.dataTask(with: url){ (data, response, error) in
        let jsonDecoder = JSONDecoder()
        if let data = data,
    //       let photoDictonary = try? jsonDecoder.decode([String : String].self, from: data){
           let photoInfo = try? jsonDecoder.decode(PhotoInfo.self, from: data){
            completion(photoInfo)
            print(photoInfo)
        }
        
        PlaygroundPage.current.finishExecution()
    }
    task.resume()
    print("KRAJ")
    
}

fetchPhotoInfo{(fetchedInfo) in
    print("OVO JE:")
    print(fetchedInfo)
}


