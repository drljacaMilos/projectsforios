import UIKit

var greeting = "Hello, playground"


import Foundation
import CryptoKit

// CryptoKit.Digest utils
extension Digest {
    var bytes: [UInt8] { Array(makeIterator()) }
    var data: Data { Data(bytes) }

    var hexStr: String {
        bytes.map { String(format: "%02X", $0) }.joined()
    }
}

func example() {
    guard let data = "hello world".data(using: .utf8) else { return }
    let digest = SHA256.hash(data: data)
    print(digest.data) // 32 bytes
    print(digest.hexStr) // B94D27B9934D3E08A52E52D7DA7DABFAC484EFE37A5380EE9088F7ACE2EFCDE9
    print(digest)
}




example()


let isoDate = "2021-06-30T16:00:00"

let dateFormatter = DateFormatter()
//dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//dateFormatter.locale = Locale(identifier: "fr_FR")
dateFormatter.locale = Locale.init(identifier: "en_GB")
dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
let date = dateFormatter.date(from:isoDate)!

print(Date())
print(date)


let currentDate = dateFormatter.string(from: Date())
print(currentDate)

