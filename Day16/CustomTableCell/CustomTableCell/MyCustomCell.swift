//
//  MyCustomCell.swift
//  CustomTableCell
//
//  Created by milos.drljaca on 5/18/21.
//

import UIKit

class MyCustomCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var networkName: UILabel!
    
    @IBOutlet weak var networkUrl: UILabel!
    
    @IBOutlet weak var numberOfUsers: UILabel!
    
}
