//
//  ViewController.swift
//  CustomTableCell
//
//  Created by milos.drljaca on 5/18/21.
//

import UIKit


class ViewController: UIViewController, UITableViewDataSource {

    
    struct SocijalnaMreza {
        let nazivMreze : String
        let logo : String //naziv fajla sa zastavom
        let urlAdresa : String
        let brojKorisnika : Int
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    let model = [
        SocijalnaMreza(nazivMreze: "Facebook", logo: "facebook.png", urlAdresa: "facebook.com", brojKorisnika: 2_000_000_000),
        SocijalnaMreza(nazivMreze: "Linkedin", logo: "linkedin.png", urlAdresa: "linkedin.com", brojKorisnika: 1_500_000_000),
        SocijalnaMreza(nazivMreze: "Skype", logo: "skype.png", urlAdresa: "mcs.skype.com", brojKorisnika: 500_000_000),
        SocijalnaMreza(nazivMreze: "Google", logo: "google.png", urlAdresa: "google.com", brojKorisnika: 700_000_000)
    ]

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as? MyCustomCell
        
        cell?.logo.image = UIImage (named : model[indexPath.row].logo)
        cell?.networkName.text = model[indexPath.row].nazivMreze
        
        cell?.networkUrl.text = model[indexPath.row].urlAdresa
        cell?.numberOfUsers.text = "\(model[indexPath.row].brojKorisnika)"
        return cell!
    }
    
    
    
    
}

