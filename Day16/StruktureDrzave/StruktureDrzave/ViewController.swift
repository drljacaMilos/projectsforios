//
//  ViewController.swift
//  StruktureDrzave
//
//  Created by milos.drljaca on 5/18/21.
//

import UIKit


struct Drzava {
    var ime : String
    var gradovi : [String]
}

class ViewController: UIViewController,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    let nizDrzava = [
        Drzava(ime: "Srbija",
               gradovi: ["Beograd", "Novi Sad", "Nis"]),
        
        Drzava(ime: "Nemacka",
               gradovi: ["Berlin", "Keln", "Minhen"]),
        
        Drzava(ime: "Rusija",
               gradovi: ["St.Petersburg", "Moskva", "Yaroslav", "Novgorod"])
    ]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return nizDrzava.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nizDrzava[section].gradovi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        cell?.textLabel?.text = nizDrzava [indexPath.section].gradovi[indexPath.row]
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nizDrzava[section].ime
    }
    
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return String(nizDrzava[section].gradovi.count)
    }
}

