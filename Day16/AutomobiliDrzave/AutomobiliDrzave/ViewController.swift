//
//  ViewController.swift
//  AutomobiliDrzave
//
//  Created by milos.drljaca on 5/18/21.
//

import UIKit

struct Automobil {
    var ime : String
    var modeli : [String]
}

class ViewController: UIViewController,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    let skupAutomobila = [
        Automobil(ime: "Audi",
                  modeli: ["j8-brzi", "h8-spori", "p0-tihi"]),
        
        Automobil(ime: "Ferari",
                  modeli: ["Mondo", "Lias", "Gargano"]),
        
        Automobil(ime: "Jugo",
                  modeli: ["Zastava", "45", "55", "Mars"])
    ]

    func numberOfSections(in tableView: UITableView) -> Int {
        return skupAutomobila.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return skupAutomobila[section].modeli.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        cell?.textLabel?.text = skupAutomobila [indexPath.section].modeli[indexPath.row]
        
        
//        cell?.backgroundColor = .cyan
        
        if indexPath.row % 2 == 0{
            
            cell?.backgroundColor = .cyan
        }
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return skupAutomobila[section].ime + ", broj modela: " + String (skupAutomobila[section].modeli.count)
    }
    
//    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
//        return String (skupAutomobila[section].modeli.count)
//    }

}

