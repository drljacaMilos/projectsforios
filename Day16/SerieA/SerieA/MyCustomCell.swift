//
//  MyCustomCell.swift
//  SerieA
//
//  Created by milos.drljaca on 5/18/21.
//

import UIKit

class MyCustomCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var stadium: UILabel!
    @IBOutlet weak var supporters: UILabel!
    
}
