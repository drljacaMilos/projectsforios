//
//  ViewController.swift
//  SerieA
//
//  Created by milos.drljaca on 5/18/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    struct SerieA{
        let name : String
        let iconPlayers : [String]
        let logo : String
        let supporters : Int
        let stadium : String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    let model = [
        SerieA(name: "Cagliari", iconPlayers: ["Fabian O'Neill", "David Suazo", "Nelson Abeijon"], logo: "cagliari.png", supporters: 50_000, stadium: "Santa Ellia"),
        SerieA(name: "Roma", iconPlayers: ["Franchesco Totti", "Marco Delvecchio", "Aldair"], logo: "roma.png", supporters: 5_000_000, stadium: "Olimpico"),
        SerieA(name: "Lazio", iconPlayers: ["Alessandro Nesta", "Paolo Di Canio", "Stefano Mauri"], logo: "lazio.png", supporters: 3_000_000, stadium: "Olimpico"),
        SerieA(name: "Juventus", iconPlayers: ["Alessandro Del Piero", "Paolo Montero", "Ciro Ferrara"], logo: "juventus.jpeg", supporters: 9_000_000, stadium: "Delle Alpi"),
        SerieA(name: "Bologna", iconPlayers: ["Giussepe Signori", "Carlo Nervo", "Keneth Anderson"], logo: "bolognia.png", supporters: 2_000_000, stadium: "Renato Dall'Ara"),
        SerieA(name: "Fiorentina", iconPlayers: ["Gabriel Omar Batistuta", "Manuel Rui Costa", "Franchesco Toldo"], logo: "fiorentina.png", supporters: 8_000_000, stadium: "Artemio Franchi"),
        SerieA(name: "Inter", iconPlayers: ["Ronaldo", "Christian Vieri", "Taribo West"], logo: "inter.png", supporters: 9_000_000, stadium: "Giussepe Meazza"),
        SerieA(name: "Parma", iconPlayers: ["Hernan Crespo", "Antonio Benarrivo", "Lilian Thuram"], logo: "parma.png", supporters: 7_000_000, stadium: "Ennio Tardinis"),
        SerieA(name: "Torino", iconPlayers: ["Marco Ferrante", "Mauro Bonomi", "Pinga"], logo: "torino.png", supporters: 3_000_000, stadium: "Olimpico")
    ]
    
    @IBOutlet weak var nameOfTheClub: UILabel!
    @IBOutlet weak var nameOfTheStadium: UILabel!
    @IBOutlet weak var supportersCount: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var teamIconPlayers: UILabel!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as? MyCustomCell
        cell?.logo.image = UIImage (named : model[indexPath.row].logo)
        cell?.name.text = model[indexPath.row].name
        cell?.stadium.text = model[indexPath.row].stadium

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        nameOfTheClub.text = "Team: \(model[indexPath.row].name)"
        nameOfTheStadium.text = model[indexPath.row].stadium
        supportersCount.text = "Support: \(String(model[indexPath.row].supporters))"
        teamLogo.image = UIImage (named : model[indexPath.row].logo)
        teamIconPlayers?.text = "Legends: \(stringFromArray(model[indexPath.row].iconPlayers))"
        
        }
    
    func stringFromArray (_ niz : [String] ) -> String {
        var stringNiz = ""
        
        for clan in niz {
            stringNiz = stringNiz + ", " + clan
        }
        stringNiz.remove(at: stringNiz.startIndex)
        return stringNiz
    }
}

