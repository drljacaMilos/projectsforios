//
//  ViewController.swift
//  FirebaseProjectNumberTwo
//
//  Created by milos.drljaca on 6/25/21.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

    private let database = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        database.child("something").observeSingleEvent(of: .value, with: { snapshot in 
            guard let value = snapshot.value as? [String: Any] else {
                return
            }
            print("Value is : \(value)")
        })
        
        let button = UIButton(frame: CGRect(x: 20, y: 200, width: view.frame.size.width-40, height: 50))
        button.setTitle("Add entry", for: .normal)
        button.backgroundColor = .link
        button.setTitleColor(.white, for: .normal)
        view.addSubview(button)
        button.addTarget(self, action: #selector(addNewEntry), for: .touchUpInside)
        
    }

    @objc private func addNewEntry() {
        let object: [String: Any] = [
            "name": "IOs Academy" as NSObject,
            "Youtube": "Yes"
        ]
        
        database.child("something_\(Int.random(in: 0..<100))").setValue(object)
    }

}

