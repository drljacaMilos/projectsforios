//
//  ViewController.swift
//  FirebaseFirestoreCrud
//
//  Created by milos.drljaca on 6/25/21.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let db = Firestore.firestore()
        
        //Adding a document
        //        db.collection("Employee").addDocument(data: ["Name" : "Milos Drljaca", "Role": "Software Developer", "Age" : 36])
        
        db.collection("Employee").addDocument(data: ["Name" : "Milan Drljaca", "Age": 40])
        
        //Adding element with completion
        //        db.collection("Employee").addDocument(data: ["Test" : "Test"]) {(error) in
        //            if let error = error {
        //
        //            }else{
        //
        //            }
        //
        //        }
        
        //Getting the document ID
        //        let newDocument = db.collection("Employee").document()
        //        newDocument.setData(["Age": 37, "Role": "CEO", "id": newDocument.documentID])
        
        //Adding a document with specific DOCUMENT ID / or replacing the data for a specific document ID
        //        db.collection("Employee").document("Document no.1").setData(["id": "jxmPSQRnTzIrW547mYZ7"])
        
        //Deleting a document
        //        db.collection("Employee").document("wzXZbbUAtMZ0AsA45kk4").delete()
        
        //Delete a field
        //        db.collection("Employee").document("jxmPSQRnTzIrW547mYZ7").updateData(["Role": FieldValue.delete()])
        //
        
        
        //Detect for errors, use completion handler
        //        db.collection("Employee").document("wzXZbbUAtMZ0AsA45kk4").delete {
        //            (error) in
        //            if error != nil {
        //
        //            }else{
        //
        //            }
        //        }
        
        
        //Read a specific document
        db.collection("Employee").document("jxmPSQRnTzIrW547mYZ7").getDocument { (document, error ) in
            if error == nil{
                if document != nil && document!.exists {
                    let documentData = document!.data()
                    
                    print("Reading data from firebase firestore: \(documentData)")
                }
            }else{
                
            }
        }
        
        //Reading collection of documents
        db.collection("Employee").getDocuments { snapshot, error in
            if error == nil && snapshot != nil {
                for document in snapshot!.documents{
                    let documentData = document.data()
                    
                    print("From the collection : \(documentData)")
                }
            }
        }
        
        //Getting a subset of document QUERY
        db.collection("Employee").whereField("Name", isEqualTo: "Milos Drljaca").getDocuments { snapshot, error in
            
            if error == nil && snapshot != nil {
                for document in snapshot!.documents{
                    let documentData = document.data()
                    
                    print("Found a match : \(documentData)")
                }
            }
        }
        
    }
    
    
}

