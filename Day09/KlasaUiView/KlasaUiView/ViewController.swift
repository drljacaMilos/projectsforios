//
//  ViewController.swift
//  KlasaUiView
//
//  Created by milos.drljaca on 5/7/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       /* let rect = CGRect(x : 10, y : 10, width: 100, height: 100)
        
        let plaviView = UIView(frame: rect)
        
        plaviView.backgroundColor = .blue
        
        
        self.view.addSubview(plaviView)
 */
        
        /*
        let crveniView = UIView(frame: CGRect( x: 10, y: 10, width: 200, height: 400 ))
        
        
        //definisanje boje UIView-a
        crveniView.backgroundColor = .red
        
        
        //definisanje rounded corners
        crveniView.layer.cornerRadius = 25
        
        
        //dodavanje border linije
        crveniView.layer.borderWidth = 10
        
        //promena boje bordera
        crveniView.layer.borderColor = UIColor.blue.cgColor
        
        self.view.addSubview(crveniView)
        */
        
        
        //U design time-u napraviti tri view-a
        /*
         prvi da bude zut i da zauzuma celu povrsinu ekrana,
         drugi da bude narandzast i da se nalazi proizvoljno sa zaobljenim ivicama plave boje,
         treci da se nalazi u zutom sa constraintima 20l, 20d, 50, 50
         
         */
        
        
        
        
        narandzasti.layer.cornerRadius = 25
        narandzasti.layer.borderColor = UIColor.blue.cgColor
        
        narandzasti.layer.borderWidth = 20
        
    }

        
    
    @IBOutlet weak var zuti: UIView!
    
    @IBOutlet weak var narandzasti: UIView!
    
    @IBOutlet weak var beli: UIView!
    
}

