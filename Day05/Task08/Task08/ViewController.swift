//
//  ViewController.swift
//  Task08
//
//  Created by milos.drljaca on 4/28/21.
//

import UIKit

class ViewController: UIViewController {
    
    var stanjeSrednjegSvetla = false
    
    var brojac = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        promeniSvetlo()
    }
    
    @IBOutlet weak var svetlo1: UIView!
    
    @IBOutlet weak var svetlo2: UIView!
    
    @IBOutlet weak var svetlo3: UIView!
    
    
    @IBOutlet weak var dugme: UIButton!
    
    @IBAction func button(_ sender: Any) {
        
        promeniSvetlo()
    }
    
    func promeniSvetlo(){
        
        if brojac == 0{
            svetlo1.backgroundColor = .green
            brojac += 1
            svetlo2.backgroundColor = .black
            svetlo3.backgroundColor = .black
            
            dugme.setTitle("Zeleno", for: .normal)
        }
        else if brojac == 1 {
            svetlo2.backgroundColor = .yellow
            brojac += 1
            svetlo1.backgroundColor = .black
            svetlo3.backgroundColor = .black
            if stanjeSrednjegSvetla{
                brojac = 0
                stanjeSrednjegSvetla = false
            }
            dugme.setTitle("Zuto", for: .normal)
        }
        else if brojac == 2{
            svetlo3.backgroundColor = .red
            brojac = 1
             
            stanjeSrednjegSvetla = true
            dugme.setTitle("Crveno", for: .normal)
        }
    }
    
}

