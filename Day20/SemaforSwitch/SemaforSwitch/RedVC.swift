//
//  ViewController.swift
//  SemaforSwitch
//
//  Created by milos.drljaca on 5/24/21.
//

import UIKit

class RedVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


  
    @IBOutlet weak var mySwitch: UISwitch!
    
    
    @IBAction func pushToYellow(_ sender: Any) {
        if mySwitch.isOn{
            performSegue(withIdentifier: "yellow", sender: nil)
        }
    }
    
    
    @IBAction func pushToGreen(_ sender: Any) {
        if mySwitch.isOn{
            performSegue(withIdentifier: "green", sender: nil)
        }
    }
    
}

