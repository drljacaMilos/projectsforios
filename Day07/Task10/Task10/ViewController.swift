//
//  ViewController.swift
//  Task10
//
//  Created by milos.drljaca on 5/5/21.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    var shortCountryNames = [
        "Argentina":"ARG",
        "Venezuela":"VEN",
        "Uruguay":"URU",
        "Serbia":"SER"
    ]
    
    
    @IBOutlet weak var inputOne: UITextField!
    @IBOutlet weak var inputTwo: UITextField!
    
    
    @IBAction func buttonAlert(_ sender: Any) {
        showAlert()
    }
    
    
    func showAlert()
    {
        for (key, value) in shortCountryNames {

            if key.uppercased() == inputOne.text!.uppercased(){
                inputTwo.text = "Shortcut for \(key.uppercased()) is \(value)"
                break
            }
            else {
                inputTwo.text = "Country does not exist"
            }
        }
        
        
        var alert = UIAlertController(title: inputOne.text, message: inputTwo.text, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Exit", style: .cancel, handler: nil))
        
        
        
        self.present(alert, animated: true)
    }
    
}

