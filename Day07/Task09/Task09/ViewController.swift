//
//  ViewController.swift
//  Task09
//
//  Created by milos.drljaca on 5/5/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBOutlet weak var inputOne: UITextField!
   
    @IBOutlet weak var inputTwo: UITextField!
    
    @IBAction func button(_ sender: Any) {
        showAlert()
    }
    
    func showAlert() {

        let alert = UIAlertController(title: inputTwo.text, message: inputOne.text, preferredStyle: .alert)


            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))

            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

            

            self.present(alert, animated: true)

        }
    
    
}

