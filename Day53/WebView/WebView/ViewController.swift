//
//  ViewController.swift
//  WebView
//
//  Created by milos.drljaca on 8.7.21..
//

import UIKit
import WebKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        webView.load("https://www.apple.com")
    }

    let webView = WKWebView()
    
    override func loadView() {
        self.view = webView

        if let url = URL(string: "https://www.apple.com") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
        if let url = URL(string: "https://www.apple.com") {
            UIApplication.shared.canOpenURL(url)
        }

    }
    
    
    @IBOutlet var button: UIButton!
    
}

extension WKWebView {
    func load(_ urlString: String){
       if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}

//??...Prikaz stranice za veb vju

//if let url = Bundle.main.url(forResource: "help", withExtension: "html"){
//    webView.loadFileURL(url, allowingReadAccessTo: url.deletingLastPathComponent())
//}

//HOW TO OPEN url in safari?

//if let url = URL(string: "https://www.apple.com") {
//    UIApplication.shared.canOpenURL(url)
//}
