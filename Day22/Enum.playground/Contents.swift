
enum CompassPoint {
    case north, east, south, west
}


var compassHeading = CompassPoint.east

var compassHeading2:CompassPoint = .east

switch compassHeading {
case .north:
    print("I am heading north.")
case .east:
    print("I am heading east.")
case .south:
    print("I am heading south.")
case .west:
    print("I am heading west.")
}
