//
//  ViewController.swift
//  HomeworkLogin
//
//  Created by milos.drljaca on 5/25/21.
//

import UIKit

class Login: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        login.isEnabled = false
    }

    @IBOutlet weak var login: UIButton!
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var rewritePassword: UITextField!
    
    
    @IBAction func changed(_ sender: Any) {
        login.isEnabled = true
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        
        let passwordValue = password.text?.description
        let repeatPassword = rewritePassword.text?.description
        
        if username.text!.count > 3 {
            if passwordValue == repeatPassword {
                performSegue(withIdentifier: "login", sender: nil)
            }else{
                showAlert("Passwords dont match.")
            }
            
        }else{
            showAlert("Username has to be minumum 4 letters")
        }
    }
    
//    @IBAction func forgotPassword(_ sender: Any) {
//        performSegue(withIdentifier: "forgotPassword", sender: nil)
//    }
    
//    @IBAction func forgotUsername(_ sender: Any) {
//        performSegue(withIdentifier: "forgotUsername", sender: nil)
//    }
    
    
    func showAlert(_ message: String){
        let alert = UIAlertController(title: "Something whent wrong", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true)
    }
}

