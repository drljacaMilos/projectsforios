//
//  MyCell.swift
//  AboutMe
//
//  Created by milos.drljaca on 6/7/21.
//

import UIKit

class MyCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBOutlet var companyName: UILabel!
    @IBOutlet var dateStartedEnded: UILabel!
    @IBOutlet var descriptionOfJob: UILabel!
    
}
