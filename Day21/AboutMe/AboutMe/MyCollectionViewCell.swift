//
//  MyCollectionViewCell.swift
//  AboutMe
//
//  Created by milos.drljaca on 6/7/21.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
   
    @IBOutlet var name: UILabel!
    
    @IBOutlet var image: UIImageView!
    
    
}
