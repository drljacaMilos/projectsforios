//
//  HobbyVC.swift
//  AboutMe
//
//  Created by milos.drljaca on 6/6/21.
//

import UIKit

class HobbyVC: UIViewController, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == sportsCollectionView ? sportArray.count : moviesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == sportsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sportsCell", for: indexPath) as? MyCollectionViewCell
            cell?.name.text = sportArray[indexPath.row][0]
            cell?.image.image = UIImage(named: sportArray[indexPath.row][1])
            return cell!
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "moviesCell", for: indexPath) as? MyCollectionViewCell
            cell?.name.text = moviesArray[indexPath.row][0]
            cell?.image.image = UIImage(named: moviesArray[indexPath.row][1])
            return cell!
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        sportsCollectionView.clipsToBounds = true
    }
    
    
    let sportArray = [
        ["Football", "football"],
        ["Table Tennis", "tabletenis"],
        ["Water polo", "waterpolo"],
        ["Basketball", "basketball"],
        ["Chess", "chess"]
    ]
    
    let moviesArray = [
        ["Predator", "predator"],
        ["Terminator 2", "terminator"],
        ["Viking", "viking"],
        ["Apocalypto", "apocalipto"],
    ]
    
    
    @IBOutlet var sportsCollectionView: UICollectionView!
    
    @IBOutlet var moviesCollectionView: UICollectionView!
    
}
