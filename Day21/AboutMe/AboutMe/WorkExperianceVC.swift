//
//  WorkExperianceVC.swift
//  AboutMe
//
//  Created by milos.drljaca on 6/6/21.
//

import UIKit

class WorkExperianceVC: UIViewController, UITableViewDataSource {
    
    
    @IBOutlet var myNewTableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workExperiance.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myNewTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MyCell
        
        
        cell?.companyName.text = workExperiance[indexPath.row][0]

        cell?.dateStartedEnded.text = workExperiance[indexPath.row][1]
        cell?.descriptionOfJob.text = workExperiance[indexPath.row][2]
        
        return cell!
        
    }
    

    let workExperiance = [
        ["Research and Development Center", "08.2018 - 09.2018", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."],
        ["Engineering Ingegneria Informatica Spa", "03.2019 - present", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
