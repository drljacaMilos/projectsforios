//
//  MiddleViewController.swift
//  OrderOfEvents
//
//  Created by milos.drljaca on 5/25/21.
//

import UIKit

class MiddleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let text = label.text{
            label.text = "\(text) \nevent Number \(eventNumber) was view DID LOAD"
        }

    eventNumber += 1
    }
    

    @IBOutlet weak var label: UILabel!
    
    var eventNumber = 1
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let text = label.text{
            label.text = "\(text) \nevent Number \(eventNumber) was view WILL APEAR"
        }
        eventNumber += 1
        
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let text = label.text{
            label.text = "\(text) \nevent Number \(eventNumber) was view DID APEAR"
        }
        eventNumber += 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let text = label.text{
            label.text = "\(text) \nevent Number \(eventNumber) was view WILL DISAPEIR"
        }
        eventNumber += 1
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let text = label.text{
            label.text = "\(text) \nevent Number \(eventNumber) was view DID DESAPEIR"
        }
        eventNumber += 1
    }
}
