//
//  SecontViewController.swift
//  ViewControllerLifeCycle
//
//  Created by milos.drljaca on 5/25/21.
//

import UIKit

class SecontViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Second ViewController - View did load")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Second View Controller - View will Appear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("Second View Controller - View did Appear")
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Second View Controller - View will desapear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("Second View Controller -  View did appear")
    }

}
