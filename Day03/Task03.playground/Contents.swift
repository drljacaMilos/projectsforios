import UIKit


var sum = 0
var number = 675

if number < 1000{
    while number > 0 {
        sum = sum + number % 10
        number = number / 10
    }
    print("Sum of figures in number is: \(sum)")
}
else{
    print("Your number is invalid.")
}

