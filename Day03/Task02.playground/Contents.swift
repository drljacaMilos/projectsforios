import UIKit

var broj :Int = 134



if broj >= Int8.min && broj <= Int8.max
{
    print("Broj je Int8.")
}
else if broj >= Int16.min && broj <= Int16.max
{
    print("Broj je Int16.")
}
else if broj >= Int32.min && broj <= Int32.max
{
    print("Broj je Int32.")
}
else
{
    print("Broj je Int64.")
}
