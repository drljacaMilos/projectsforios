//
//  ViewController.swift
//  Homework
//
//  Created by milos.drljaca on 5/17/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serieA.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        cell.textLabel?.text = serieA[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        input.text = serieA[indexPath.row]
        }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.dataSource = self
        myTableView.delegate = self
    }

    
    var serieA = ["Cagliari", "Napoli", "Torino", "Inter", "Roma", "Lazio"]

    @IBOutlet weak var myTableView: UITableView!
    
    @IBOutlet weak var input: UITextField!
    
    
    @IBAction func add(_ sender: Any) {
        serieA.append(input.text!)
        myTableView.reloadData()
    }
    
    
    @IBAction func updateLine(_ sender: Any) {
        
        if let rowToUpdate = myTableView.indexPathForSelectedRow?.row {
            serieA[rowToUpdate] = input.text!
            myTableView.reloadData()
        }
        
    }
    
    @IBAction func deleteLine(_ sender: Any) {
        if let rowToDelete = myTableView.indexPathForSelectedRow?.row {
            serieA.remove(at: rowToDelete)
            myTableView.reloadData()
        }
        
    }
    
    
    @IBAction func moveUp(_ sender: Any) {
        if var rowToMove = myTableView.indexPathForSelectedRow?.row {
            if rowToMove > 0{
                let element = serieA.remove(at: rowToMove)
                rowToMove = rowToMove - 1
                serieA.insert(element, at: rowToMove)
            }
            myTableView.reloadData()
        }
    }
    
    
    @IBAction func moveDown(_ sender: Any) {
        if var rowToMove = myTableView.indexPathForSelectedRow?.row {
            if rowToMove < serieA.count - 1{
                print(rowToMove)
                print(serieA.count)
                let element = serieA.remove(at: rowToMove)
                rowToMove = rowToMove + 1
                serieA.insert(element, at: rowToMove)
            }
            myTableView.reloadData()
        }
    }
    
}

