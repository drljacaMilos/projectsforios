//
//  ViewController.swift
//  Automobili
//
//  Created by milos.drljaca on 5/17/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return automobili.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        cell.textLabel?.text = automobili[indexPath.row]
        
        return cell    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        input.text = automobili[indexPath.row]
        }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.dataSource = self
        myTableView.delegate = self
    }

    @IBOutlet weak var myTableView: UITableView!
    
//    let automobili = ["Jugo", "Dacia", "Bmw", "Audi", "Ferrari", "Fiat", "Neki","Neki","Neki","Neki","Neki","Neki","Neki","Neki","Neki"]
    
    var automobili = ["Jugo", "Dacia", "Bmw", "Audi", "Ferrari", "Fiat" ]

    @IBOutlet weak var input: UITextField!
    
    
    @IBAction func add(_ sender: Any) {
        automobili.append(input.text!)
        myTableView.reloadData()
    }
    
   
    @IBAction func deleteLine(_ sender: Any) {
        if let rowToBeDeleted =  myTableView.indexPathForSelectedRow?.row{
                automobili.remove(at: rowToBeDeleted)
                myTableView.reloadData()
        }
    }
    
    
    
    @IBAction func updateLine(_ sender: Any) {
        if let rowToBeUpdated = myTableView.indexPathForSelectedRow?.row{
            automobili[rowToBeUpdated] = input.text!
        myTableView.reloadData()
        }
    }
}

