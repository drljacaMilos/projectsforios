//
//  ViewController.swift
//  Demo02
//
//  Created by milos.drljaca on 4/22/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBOutlet weak var mojaLabela: UILabel!
    
    
    @IBOutlet weak var mojTekst: UITextField!
    
    @IBAction func klik(_ sender: Any) {
        mojaLabela.text = mojTekst.text    }
    
    
}

