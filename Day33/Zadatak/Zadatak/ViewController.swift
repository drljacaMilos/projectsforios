//
//  ViewController.swift
//  Zadatak
//
//  Created by milos.drljaca on 6/10/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        createTextField()
        createButton()
    }

    var someText = UITextField()
    var objects: [UIView] = []
    var nextYPosition: Int = 50
    
    
    func createTextField() {
        someText.frame = CGRect(x: 80, y: 300, width: 150, height: 50)
        someText.backgroundColor = .red
        someText.contentMode = .center
        view.addSubview(someText)
    }

    func createButton() {
        let someButton = UIButton()
        
        someButton.frame = CGRect(x: 80, y: 400, width: 150, height: 50)
        someButton.backgroundColor = .blue
        someButton.contentMode = .center
        someButton.setTitle("Make objects", for: .normal)
        someButton.addTarget(self, action: #selector(ViewController.makeObjects), for: .touchUpInside)
        view.addSubview(someButton)
    }
    
    @objc func makeObjects(){
        
        if let number = Int(someText.text!){
            
            for index in 0...number - 1{
                
                let v = UIView()
                    v.frame = CGRect(x: 100, y:  nextYPosition, width: 150, height: 25)
                        v.backgroundColor = .yellow
                    nextYPosition += 30

                objects.append(v)
                
            }
        }else if String(someText.text!) == ""
        {
            for object in objects{
                object.removeFromSuperview()
            }
            nextYPosition = 50
            
            objects.removeAll()
            
            self.showAlert("Try again with number!")
        }
        
        print(objects.count)
        someText.text?.removeAll()
        
        for object in objects {
            self.view.addSubview(object)
        }
      
    }
        

    
    func showAlert(_ message: String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        self.present(alert, animated: true)
    }
}

