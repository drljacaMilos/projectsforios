//
//  PlacesAndAudiExperienceVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 13.7.21..
//

import UIKit

class PlacesAndAudiExperienceVC: AudiEventiVC {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBar()

        if vlaueOfDualViewControllerToShow == 1{
            setAudiExperianceData()
        }else if vlaueOfDualViewControllerToShow == 2 {
            setPlacesAndTeritoryData()
        }
    }
    
    //Slider button for images
    @IBAction func leftClickAction(_ sender: Any) {
        if currentSliderImage >  0 {
            currentSliderImage -= 1
        }else{
            currentSliderImage = sizeOfImagesForSlider
        }
        sliderImage.image = arraySliderImages[currentSliderImage]

    }
    
    //Slider button for images
    @IBAction func rightClickAction(_ sender: Any) {
        if currentSliderImage < sizeOfImagesForSlider{
            currentSliderImage += 1
        }else{
            currentSliderImage = 0
        }
        sliderImage.image = arraySliderImages[currentSliderImage]
    }
    
    
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var image: UIImageView!
    @IBOutlet var eventTitle: UILabel!
    @IBOutlet var eventText: UILabel!
    @IBOutlet var eventDescription: UILabel!
    
    
    @IBOutlet var textView: UITextView!
    @IBOutlet var sliderImage: UIImageView!
    
    var arraySliderImages: [UIImage] = []
    var currentSliderImage: Int = 0
    var sizeOfImagesForSlider: Int = 0
    
    
    func setAudiExperianceData(){
        
        let audiExperiance = TestData.audiDriveExperience
        
        sizeOfImagesForSlider = (audiExperiance?.sliderImage.count)! - 1
        
        titleLabel.text = audiExperiance?.titleAde
        eventTitle.text = audiExperiance?.titleAde
        eventText.text = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
        eventDescription.text = audiExperiance?.subtitleAde
        textView.text = audiExperiance?.description
        
        if let urlString = audiExperiance?.imageAde.href, let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){ (data,response,error) in
                if let data = data{
                    DispatchQueue.main.async {
                        self.image.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        
        if let arrayOfImages = TestData.audiDriveExperience?.sliderImage.count{
            for i in 0...arrayOfImages - 1 {
                if let urlString = audiExperiance?.sliderImage[i].href, let url = URL(string: urlString){
                    URLSession.shared.dataTask(with: url){ (data,response,error) in
                        if let data = data{
                            DispatchQueue.main.async {
                                if i == 0 {
                                    self.sliderImage.image = UIImage(data: data)
                                }
                                self.arraySliderImages.append(UIImage(data: data)!)
                                
                            }
                        }
                        
                    }.resume()
                }
            }
            
        }

    }
    
    func setPlacesAndTeritoryData(){
        
        let placesAndTeritory = TestData.placesAndTerritory
        
        sizeOfImagesForSlider = (placesAndTeritory?.placesSlider.count)! - 1
        
        titleLabel.text = placesAndTeritory?.title
        eventTitle.text = placesAndTeritory?.placeTitle
        eventText.text = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
        eventDescription.text = placesAndTeritory?.placeSubtitle
        textView.text = placesAndTeritory?.description
        
        if let urlString = placesAndTeritory?.imagePlace.href, let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){ (data,response,error) in
                if let data = data{
                    DispatchQueue.main.async {
                        self.image.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        
        if let arrayOfImages = TestData.placesAndTerritory?.placesSlider.count{
            for i in 0...arrayOfImages - 1 {
                if let urlString = placesAndTeritory?.placesSlider[i].href, let url = URL(string: urlString){
                    URLSession.shared.dataTask(with: url){ (data,response,error) in
                        if let data = data{
                            DispatchQueue.main.async {
                                if i == 0 {
                                    self.sliderImage.image = UIImage(data: data)
                                }
                                self.arraySliderImages.append(UIImage(data: data)!)
                                
                            }
                        }
                        
                    }.resume()
                }
            }
            
        }
    }
}
