//
//  Helper.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import Foundation
import UIKit


class AudiEventiVC: UIViewController{
    
    var userIsAuthenticated: Bool = true
    var hasNotification: Bool = true
    
    var vlaueOfDualViewControllerToShow: Int?

    
    //Prepare navigation bar for burgerVC
    func prepareNavigationBar(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "...", style: .done, target: self, action: #selector(showBurgerVC))
        navigationItem.leftBarButtonItem?.tintColor = .black
        navigationItem.leftBarButtonItem?.image = UIImage(named: "menu")
        
        navigationItem.rightBarButtonItem = notificationRightBar()
        
        navigationController?.navigationBar.isHidden = false
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "audi_logo")
        navigationItem.titleView = imageView
    }
    
    //Present burger for View controller
    func presentDetail(_ vc: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.7
        view.window?.layer.add(transition, forKey: kCATransition)
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false, completion: nil)
    }
    
    //Show burgerVC
    @objc func showBurgerVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: ConfigData.burgerMenuViewController)
        presentDetail(vc)
    }
    
    //Gesture dissmis for burgerVC
    func burgerVcGestureDissmis(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(BurgerVC.tappedOnScreen(_:)))
        view.addGestureRecognizer(tap)
    }
    
    //Gesture dissmis for burgerVC
    @objc func tappedOnScreen(_ sender: UIGestureRecognizer) {
        let tapped = sender.location(in: view)
        let screenWidth = UIScreen.main.bounds.width
        if  tapped.x > screenWidth - 80 && tapped.y > 0 {
            let transition = CATransition()
            transition.duration = 0.3
            view.window?.layer.add(transition, forKey: kCATransition)
            dismiss(animated: false, completion: nil)
        }
    }
    
    //Alert
    @objc func showAlertForCoupon() {
        let alert = UIAlertController(title: "Coupon", message: "Enter Coupon number: ", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Some default text"
        }
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        self.present(alert, animated: true)
    }
    
    //Navigate from menu
    @objc func navigateTo( sender: ButtonInMenu) {

        switch sender.buttonKey {
        case ConfigData.menuItems.SIDE_MENU_GUEST_HOME_PAGE.rawValue:
            
            if userIsAuthenticated{
                showViewController(identifier: ConfigData.homePremiumViewController, positionOfDualViewControllerToShow: 0)
            }else{
                showViewController(identifier: ConfigData.homeGuestViewController, positionOfDualViewControllerToShow: 0)
            }
        case ConfigData.menuItems.SIDE_MENU_GUEST_EVENT_LIST.rawValue:
            showViewController(identifier: ConfigData.eventiListViewController, positionOfDualViewControllerToShow: 0)
            
        case ConfigData.menuItems.SIDE_MENU_GUEST_COUPON_LOGIN.rawValue:
            showAlertForCoupon()
            
        case ConfigData.menuItems.SIDE_MENU_GUEST_SETTINGS.rawValue:
            showViewController(identifier: ConfigData.settingsViewController, positionOfDualViewControllerToShow: 0)
            
        case ConfigData.menuItems.SIDE_MENU_PREMIUM_EVENT_INFO.rawValue:
            showViewController(identifier: ConfigData.infoAndContactsViewController, positionOfDualViewControllerToShow: 0)
            
        case ConfigData.menuItems.SIDE_MENU_PREMIUM_ADE.rawValue:
            showViewController(identifier: ConfigData.placesAndAudiExperieanceViewController, positionOfDualViewControllerToShow: 1)
            
        case ConfigData.menuItems.SIDE_MENU_PREMIUM_LOCATION.rawValue:
            showViewController(identifier: ConfigData.placesAndAudiExperieanceViewController, positionOfDualViewControllerToShow: 2)
            
        case ConfigData.menuItems.SIDE_MENU_PREMIUM_FOOD_EXPERIENCE.rawValue:
            showViewController(identifier: ConfigData.foodExperienceAndEventProgramViewController, positionOfDualViewControllerToShow: 3)
            
            
        case ConfigData.menuItems.SIDE_MENU_PREMIUM_EVENT_DETAIL.rawValue:
            showViewController(identifier: ConfigData.foodExperienceAndEventProgramViewController, positionOfDualViewControllerToShow: 4)
            
        default:
            return
        }        
    }
    
    //Navigate to View Controller
    @objc func showViewController(identifier: String, positionOfDualViewControllerToShow: Int) {
       
        let nextViewController: AudiEventiVC
        
        if positionOfDualViewControllerToShow > 0 && positionOfDualViewControllerToShow < 3 {
            nextViewController = storyboard?.instantiateViewController(identifier: identifier) as! PlacesAndAudiExperienceVC
            nextViewController.vlaueOfDualViewControllerToShow = positionOfDualViewControllerToShow
        }else if positionOfDualViewControllerToShow > 2 {
            nextViewController = storyboard?.instantiateViewController(identifier: identifier) as! FoodExperienceAndEventProgramVC
            nextViewController.vlaueOfDualViewControllerToShow = positionOfDualViewControllerToShow
            
        }else{
            nextViewController = storyboard?.instantiateViewController(identifier: identifier) as! AudiEventiVC
        }
        
        let nc = UINavigationController(rootViewController: nextViewController)
        nc.modalPresentationStyle = .fullScreen
        self.present(nc, animated: false)
    }
    
    //Show notification
    func notificationRightBar() -> UIBarButtonItem? {
        
        let notification: String
        
        if userIsAuthenticated {
            
            if hasNotification {
                notification = ConfigData.notificationBadge
            }else{
                notification = ConfigData.notificationNoBadge
            }
            let rightButton = UIButton(type: .custom)
            rightButton.setImage(UIImage(named: notification), for: .normal)
            rightButton.frame = CGRect(x: 30, y: 0, width: 10, height: 10)
            rightButton.addTarget(self, action: #selector(showNotification), for: .touchUpInside)
            
            let barItem = UIBarButtonItem(customView: rightButton)
            _ = barItem.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
            _ = barItem.customView?.heightAnchor.constraint(equalToConstant: 20).isActive = true
            
            return barItem
        }
        return nil
    }
    
    //Navigate to
    @objc func showNotification() {
        showViewController(identifier: ConfigData.notificheViewController, positionOfDualViewControllerToShow: 0)
    }
    
}
