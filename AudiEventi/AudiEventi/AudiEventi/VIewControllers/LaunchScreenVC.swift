//
//  LaunchScreenVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 6.7.21..
//

import UIKit

class LaunchScreenVC: AudiEventiVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader.startAnimating()
        
        navigationController?.navigationBar.isHidden = true
        
        //simulation waiting for auth.
        DispatchQueue.main.async {
            TestData.populateTestData()
            FetchedData.fechMenuElementName()
        }
        
        if userIsAuthenticated{
            self.performSegue(withIdentifier: ConfigData.homePremiumSegue, sender: self)
        }else{
            self.performSegue(withIdentifier: ConfigData.homeGuestSegue, sender: self)
        }
    }
    
    @IBOutlet var loader: UIActivityIndicatorView!
    
    
    
    
}
