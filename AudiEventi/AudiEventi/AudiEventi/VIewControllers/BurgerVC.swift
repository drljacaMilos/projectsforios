//
//  BurgerVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import UIKit

class BurgerVC: AudiEventiVC, UITableViewDelegate, UITableViewDataSource{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        burgerVcGestureDissmis()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        menuBurgerTableView.tableFooterView = UIView()

    }

    let menuGuestArray = FetchedData.sideMenuGuest
    let menuPremiumArray = FetchedData.sideMenuPremium
    
    @IBOutlet var menuBurgerTableView: UITableView!
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    //Table view count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuPremiumArray?.count ?? 0
    }
    
    //Table view cell for row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: ConfigData.burgerMenuCell, for: indexPath) as! CellForMenuTV
        
        var menuValue: [String]?
        
        if userIsAuthenticated{
            if let values = menuPremiumArray?[indexPath.row]{
                menuValue = values
            }
        }else{
            if indexPath.row < 9 && indexPath.row > 2{
                cell.separatorInset = UIEdgeInsets(top: 0.0, left: cell.bounds.size.width, bottom: 0.0, right: 0.0);
                return cell
            }else {
                if indexPath.row == 9{
                    menuValue = menuGuestArray?[3]
                }else{
                    menuValue = menuGuestArray?[indexPath.row]
                }
            }
        }

        cell.buttonBurgerMenu.buttonKey = menuValue?[0]
        cell.buttonBurgerMenu.setTitle(menuValue?[1], for: .normal)
        cell.buttonBurgerMenu.setImage(UIImage(named: menuValue?[0] ?? ""), for: .normal)
        cell.buttonBurgerMenu.addTarget(self, action: #selector(navigateTo), for: .touchUpInside)
        
        return cell
    }
    
    //Override height of the table view cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(40)
    }

    
}
