//
//  File.swift
//  AudiEventi
//
//  Created by milos.drljaca on 5.7.21..
//

import Foundation


class FetchedData{
    
    static var elementsMenuNameList: Dictionary<String, String>? = Dictionary()
    static var sideMenuGuest: [Int : [String]]? = [:]
    static var sideMenuPremium: [Int : [String]]? = [:]
    
    //Data for Langs
    static func fechMenuElementName(){
        DataPreparation.fetchLangs(url: DataPreparation.langUrl) { result in
            guard let result = result else { return }

            var elementGuestCount = 0
            var elementPremiumCount = 0
            
            
            //Mapping properties of object to dictionaries 
            for property in Mirror(reflecting: result).children {
                
                let label = property.label as! String
                let value = property.value as! String
                
                
                if label.description.range(of: "SIDE_MENU_GUEST") != nil{
                    
                    FetchedData.sideMenuGuest?.updateValue([label, value], forKey: elementGuestCount)
                    FetchedData.sideMenuPremium?.updateValue([label, value], forKey: elementPremiumCount)

                    elementGuestCount += 1
                    elementPremiumCount += 1
                    
                }else if label.range(of: "SIDE_MENU_PREMIUM") != nil {
                    FetchedData.sideMenuPremium?.updateValue([label, value], forKey: elementPremiumCount)
                    elementPremiumCount += 1
                }else{
                    FetchedData.elementsMenuNameList?[label.description] = value as! String
                }
            }
//            print(FetchedData.sideMenuPremium?.keys)
//            print(FetchedData.sideMenuPremium?.values)
//            print(FetchedData.sideMenuPremium?.count)
        }
    }
    
}
