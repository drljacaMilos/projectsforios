//
//  DataPreparation.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import Foundation
import UIKit


class DataPreparation {
    
    //URL's
    static let langUrl = "http://mobileapp-coll.engds.it/AudiEventi/langs/it_IT.json"
    
    
    //Fetching Lang data
    static func fetchLangs(url: String, completion: @escaping (LangModel?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing Lang Api: \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                return
            }
            
            if let data = data, case let langs = try? JSONDecoder().decode(LangModel.self, from: data){
                completion(langs)
            }
        }
        task.resume()
    }
    
    
    
    
    
    
}

