//
//  FoodExperianceModel.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import Foundation


struct FoodExperience: Codable, ResultProtocol {
    var data: [FoodExperienceData] // is it array of food data?
    var result: String
    var resultCode: String
    var resultMessage: String
    
    //Add enum keys for all structures
}

struct FoodExperienceData: Codable {
    var id: String
    var title: String
    var header: String
    var foodSubtitle: FoodSubtitle
    
    var foodImage: BackgroundImage // same from public event
    var programExperience: [ProgramExperience]
}


struct FoodSubtitle: Codable {
    var value: String
    var format: String
    var processed: String
}

struct ProgramExperience: Codable {
    
    var day: String
    var start: String
    var type: String
    var activity: String
    var site: String
    var description: String //formated html
    var food: String //formated html
    var alergens: String //formated html
}
