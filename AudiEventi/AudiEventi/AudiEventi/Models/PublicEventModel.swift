//
//  Model.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import Foundation

struct PublicEvent: Codable{
    var id: String
    var title: String
    var description: PublicDescription
    var dataEvent: String
    var linkMyAudi: LinkMyAudi
    var priority: Int
    var status: String
    var backgroundImage: BackgroundImage?
    
    //Add enum keys for all structures
    
//    enum CodingKeys: String, CodingKeys{
//                    case id = ""
//                    case title = ""
//                    case description = ""
//                    case dataEvent = ""
//                    case linkMyAudi = ""
//                    case priority = ""
//                    case status = ""
//                    case backgroundImage = ""
//    }
  
}

struct PublicDescription: Codable{
    var format: String
    var value: String
}

struct LinkMyAudi: Codable{
    var uri: String
    var title: String
    var options: [Option]
}

//What is an Option?
struct Option: Codable{
    
}

struct BackgroundImage: Codable{
    var id: String
    var href: String
    var meta: Meta
}

struct Meta: Codable{
    var alt: String
    var title: String
    var width: Int
    var height: Int
}
