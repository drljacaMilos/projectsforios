//
//  CellForEventiListTV.swift
//  AudiEventi
//
//  Created by milos.drljaca on 7.7.21..
//

import UIKit

class CellForEventiListTV: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet var eventiImage: UIImageView!
    @IBOutlet var eventiTitle: UILabel!
    @IBOutlet var eventiDescription: UILabel!
    @IBOutlet var eventiButton: UIButton!
    
    
}
