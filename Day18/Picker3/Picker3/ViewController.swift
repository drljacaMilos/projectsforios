//
//  ViewController.swift
//  Picker3
//
//  Created by milos.drljaca on 5/20/21.
//

import UIKit

class ViewController: UIViewController,
                      UIPickerViewDelegate,
                      UIPickerViewDataSource  {

    //Global variable
    var countryNumber = 0
    
    //Structure
    struct Country {
        var name: String
        var city: [String]
    }
    
    //Data model
    var countries = [
        Country(name: "Serbia", city: ["Beograd", "Novi Sad", "Nis", "Kragujevac"]),
        Country(name: "Italy", city:["Cagliari", "Palermo", "Napoli", "Milano"]),
        Country(name: "England", city: ["London", "Newcastle","Liverpool","Oxford"]),
        Country(name: "Russia", city: ["Moskva", "Novgorod", "Arhangelsk", "Voldoga","Omsk", "Tomsk", "Yaroslav"]),
        Country(name: "Usa", city: ["New York", "Boston", "Los Angeles", "Michigan","Detroit", "Washington"])
    ]
    
    
    //Picker View
    @IBOutlet weak var pickerView: UIPickerView!
    
    //Labels
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCity: UILabel!
    
    //Text-fields
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    //Outlets for Buttons
    @IBOutlet weak var addCountryOutlet: UIButton!
    @IBOutlet weak var deleteCountryOutlet: UIButton!
    @IBOutlet weak var addCityOutlet: UIButton!
    @IBOutlet weak var DeleteCityOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        setButtons(addCountryOutlet)
        setButtons(deleteCountryOutlet)
        
        setButtons(addCityOutlet)
        setButtons(DeleteCityOutlet)
       
    }

    //Number of components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
        
    }
    
    //Number of Rows in Component
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
        if component == 0{
            return countries.count
        }
        else{
            return citiesFromCountry(countryNumber).count
        }
    }
    
    //Title
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if component == 0{
            return countries[row].name
        }
        else{
            return citiesFromCountry(countryNumber)[row]
        }
    }
    
    //Delegate
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if component == 0{
            countryTextField.text = countries[row].name
            countryNumber = row
        }
        else{
            cityTextField.text = citiesFromCountry(countryNumber)[row]
        }
        print(countryNumber)
        print(countries.count)
        pickerView.reloadComponent(1)
    }
    
    func citiesFromCountry (_ number : Int) ->[String]{

        if number < countries.count && number > -1  {
            return countries[number].city
        }
        return [""]
    }
    
    //Functions for Country
    @IBAction func addCountry(_ sender: Any) {
        if countries.contains(where: {$0.name == countryTextField.text!}){
            print("Nothing happens.")
        }else{
            var f = Country(name: "", city: [])
            f.name = String(countryTextField.text!)
            countries.append(f)
            clearState()
            pickerView.reloadAllComponents()
        }
        
    }
    
    @IBAction func RemoveCountry(_ sender: Any) {
        if countries.contains(where: {$0.name == countryTextField.text!}){
            if let nameOfCountry = countryTextField.text{
                countries.removeAll(where:{$0.name == nameOfCountry})
                pickerView.selectRow(1, inComponent: 0, animated: true)

                clearState()
                pickerView.reloadAllComponents()
            }
        }else{
            print("No element.")
        }
    }
    
    //Functions for City
    @IBAction func addCity(_ sender: Any) {
      
        if countries[countryNumber].city.contains(where: {$0 == cityTextField.text!}){
            print("Nothing happens.")
        }else{
            if let nameOfCity = cityTextField.text{
                countries[countryNumber].city.append(nameOfCity)
                clearState()
                pickerView.reloadAllComponents()
        }
    }
    }
    
    @IBAction func deleteCity(_ sender: Any) {
        if countries[countryNumber].city.contains(where: {$0 == cityTextField.text!}){
            
            if let nameOfCity = cityTextField.text{
                countries[countryNumber].city.removeAll(where:{$0 == nameOfCity})
                pickerView.selectRow(1, inComponent: 1, animated: true)
                clearState()
                pickerView.reloadAllComponents()
            }else{
                print("No element.")
            }
    }
    }
    
    //Set Buttons function
    func setButtons (_ button:UIButton){
        
        button.backgroundColor = .red
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    //Clear state for country and city
    func clearState(){
        cityTextField.text = ""
        countryTextField.text = ""
    }
}

