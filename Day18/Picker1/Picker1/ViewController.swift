//
//  ViewController.swift
//  Picker1
//
//  Created by milos.drljaca on 5/20/21.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBOutlet weak var text: UITextField!
    
    @IBOutlet weak var pickerCountries: UIPickerView!
    
    
    let countries = ["Serbia", "Bosnia", "Russia", "Romania", "Bulgaria", "Ukraine", "Greece", "Armenia", "Georgia", "Tunis"]
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return countries[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        text.text = countries[row]
    }
}

