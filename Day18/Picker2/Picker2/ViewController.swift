//
//  ViewController.swift
//  Picker2
//
//  Created by milos.drljaca on 5/20/21.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    

    
    let vozila = ["Mercedes", "Bmw", "Audi", "Porche", "Lexus"]
    
    let boje = ["Bela", "Plava", "Crvena", "Ljubicasta", "Crna"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var picker: UIPickerView!
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return vozila.count
        }
        else{
            return boje.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return vozila[row]
            print(vozila[row])
        }else{
            return boje[row]
        }
    }
    
    
}

