//
//  ViewController.swift
//  Gesture4
//
//  Created by milos.drljaca on 6/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addPanGesture()
        print(imageView.frame.origin.x)
        print(imageView.frame.origin.y)
    }

    @IBOutlet var imageView: UIImageView!
    
    
    
    func addPanGesture(){
        
        imageView.isUserInteractionEnabled = true
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(ViewController.handlePan(_:)))
        self.imageView.addGestureRecognizer(pan)
    }
    
    
    var startPoint: CGPoint?
    
    @objc func handlePan(_ sender: UIPanGestureRecognizer){
        if startPoint == nil {
            startPoint = sender.location(in: view)
        }
        
        print(imageView.frame.origin.x)
        let dx = sender.location(in: view).x - startPoint!.x
        let dy = sender.location(in: view).y - startPoint!.y
        
        
        imageView.frame.origin = CGPoint(x: imageView.frame.origin.x + dx, y: imageView.frame.origin.y + dy)
        
        startPoint = sender.location(in: view)
        
        
        if sender.state == .ended {
            startPoint = nil
            imageView.frame.origin = CGPoint(x: 87, y: 295)
        }
        
        print("DX \(dx)")
        print("DY \(dy)")
    }
    
    

}

