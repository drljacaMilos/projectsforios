//
//  ViewController.swift
//  Gesture3
//
//  Created by milos.drljaca on 6/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBOutlet var zutiView: UIView!
    
    
    @IBAction func handleTap(_ sender: UITapGestureRecognizer) {
        print("Dogodio se Tap")
    }
    
    
    @IBAction func handleYellowTap(_ sender: Any) {
        
        let g = sender as! UIGestureRecognizer
        let lokacijaZuti = g.location(in: zutiView)
        let lokacijaVeliki = g.location(in: self.view)
        print("Dogodio se tap", lokacijaZuti, lokacijaVeliki)
    }
}

