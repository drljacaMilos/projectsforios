//
//  ViewController.swift
//  Gesture2
//
//  Created by milos.drljaca on 6/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.makeTap(_:)))
        view.addGestureRecognizer(tap)
    }

    
    
    
    @objc func makeTap(_ sender: UIGestureRecognizer) {
        print("Dogodio se tap ", sender.location(in: view))
    }

}

