//
//  ViewController.swift
//  Gesture5
//
//  Created by milos.drljaca on 6/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = backgorund
       
        //tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ViewController.tapAction(sender:)))
        
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view.addGestureRecognizer(tapGesture)
        
        
        //pinch gesture
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(ViewController.pinchAction(sender:)))
        view.addGestureRecognizer(pinchGesture)
        
        //rotate gesture
        
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(ViewController.rotateAction(sender:)))
        
        view.addGestureRecognizer(rotateGesture)
        
        //swipe gesture
        
        
    }

    
    
    @IBOutlet var labelState: UILabel!
    
    let backgorund = UIColor(red: 1.0, green: 0.98, blue: 0.96, alpha: 1.0)
    
    
    
    //tapAction
    
    @objc func tapAction(sender: UITapGestureRecognizer){
        if sender.state == .ended{
            labelState.text = "Tapped"
            view.backgroundColor = .red
        }
    }
    
    //pinchAction\
    
    @objc func pinchAction(sender:UIPinchGestureRecognizer){
        if sender.state == .began{
            labelState.text = "Pinch began!"
            view.backgroundColor = UIColor.cyan
        }
    }
    
    //rotateAction
    
    @objc func rotateAction(sender: UIRotationGestureRecognizer){
        if sender.state == .began {
            labelState.text = "Rotate began!"
            view.backgroundColor = UIColor.blue
        }
        if sender.state == .changed{
            labelState.text = "Rotate Changed"
            view.backgroundColor = UIColor.green
        }
        if sender.state == .ended{
            labelState.text = "Rotate Ended"
            view.backgroundColor = UIColor.yellow
        }
        
    }
    
    //swipeAction
    
    
}

