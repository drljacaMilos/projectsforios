import UIKit


//Definisanje Dictionary-a na tri nacina

var recnik1 = [String:String]()

var recnik2 : [String:Int] = [:]

var recnik3 : Dictionary<Int,String> = [:]


var valute = ["EUR":"Euro","RSD":"Dinar","USD":"Dolar"]

//print(valute["RSD"]!)
//
//print(valute["CAD"])
//
//let v1 = valute["USD"]
//print(v1)


//iteracija za Dictionary


//1. Iteracija kroz kljuceve
//for i in valute.keys{
//    print(i)
//}

//2.Iteracija kroz vrednosti
//for c in valute.values{
//    print(c)
//}




//var responseMessages = [200:"Ok",403:"Access forbidden",404:"File not found", 500:"Internal server error"]
//
//let httpResponseCodes = [200,403,301]
//
//for c in httpResponseCodes{
//    if let message = responseMessages[c]{
//        print("Response: \(c) : \(message)")
//    }else{
//        print("Unknown response : \(c)")
//    }
//
//}

//pozivanje dictionarija i dodeljivanje nove vrednosti nismo ga dodali u dictionary?
//responseMessages[301] = "Moved permanently"
//
//print(responseMessages[301])



// 1. Data je celobrojna vrednost veca ili jednaka od nule a manja ili jednaka od 9. Ispisati recima tu cifru.

var broj = 6

var brojevi1 = [
    0:"Nula",
    1:"Jedan",
    2:"Dva",
    3:"Tri",
    4:"Cetri",
    5:"Pet",
    6:"Sest",
    7:"Sedam",
    8:"Osam",
    9:"Devet",
]


print("Zadatak broj 1: Tekstualna vrednost broja \(broj), je : \(brojevi1[broj]!)")


// 2. Data je celobrojna vrednost veca ili jednaka od nule a manja od 99. Ispisati recima tu cifru.


broj = 40

var brojevi2 = [
    0:"Nula",
    1:"Jedan",
    2:"Dva",
    3:"Tri",
    4:"Cetri",
    5:"Pet",
    6:"Sest",
    7:"Sedam",
    8:"Osam",
    9:"Devet",
    10:"Deset",
    11:"Jedanaest",
    12:"Dvanaest",
    13:"Trineast",
    14:"Cetrnaest",
    15:"Petnaest",
    16:"Sesnaest",
    17:"Sedamnaest",
    18:"Osamnaest",
    19:"Devetnaest",
    20:"Dvadeset",
    30:"Trideset",
    40:"Cetrdeset",
    50:"Pedeset",
    60:"Sezdeset",
    70:"Sedamdeset",
    80:"Osamdeset",
    90:"Devedeset"
]

for n in brojevi2{
    
    var konacanBroj = ""
    
    
    let desetice = (broj / 10) * 10
    let jedinice = broj % 10
    
    let rezultat = desetice + jedinice
    
    var dodatniBrojevi = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
    
    if dodatniBrojevi.contains(rezultat) {
        konacanBroj = String (brojevi2[rezultat]!)
    }
    else
    {
        if jedinice == 0{
            konacanBroj =  String ( brojevi2 [desetice]!)
        }
        else{
            konacanBroj =  String ( brojevi2 [desetice]!) + " " + String (brojevi2 [jedinice]!)
        }
        
        
    }
    
    //    print(desetice, jedinice)
   
    print("Zadatak broj 2: Tekstualna vrednost broja \(broj), je : \(konacanBroj)")
    break
    
}


// 3. Data je celobrojna vrednost veca ili jednaka od nule a manja od 999. Ispisati recima tu cifru.

broj = 234

var brojevi3 = [
    0:"Nula",
    1:"Jedan",
    2:"Dva",
    3:"Tri",
    4:"Cetri",
    5:"Pet",
    6:"Sest",
    7:"Sedam",
    8:"Osam",
    9:"Devet",
    10:"Deset",
    11:"Jedanaest",
    12:"Dvanaest",
    13:"Trineast",
    14:"Cetrnaest",
    15:"Petnaest",
    16:"Sesnaest",
    17:"Sedamnaest",
    18:"Osamnaest",
    19:"Devetnaest",
    20:"Dvadeset",
    30:"Trideset",
    40:"Cetrdeset",
    50:"Pedeset",
    60:"Sezdeset",
    70:"Sedamdeset",
    80:"Osamdeset",
    90:"Devedeset",
    100:"Sto",
    200:"Dvesta",
    300:"Trista",
    400:"Cetristo",
    500:"Petsto",
    600:"Sesto",
    700:"Sedamsto",
    800:"Osamsto",
    900:"Devetsto"
]



for n in brojevi3{
    
    var konacanBroj = ""
    
    let stotine = (broj / 100) * 100
    let desetice = (broj  % 100 / 10) * 10
    let jedinice = broj % 10
 
    
    let rezultat =  desetice + jedinice
    
    var dodatniBrojevi = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]

    if dodatniBrojevi.contains(rezultat) {
        if stotine != 0 {
            if rezultat == 0{
                konacanBroj = String(brojevi3[stotine]!)
            }else{
                konacanBroj = String(brojevi3[stotine]!) + " " + String (brojevi3[rezultat]!)
            }
            
        }else{
            konacanBroj = String (brojevi3[rezultat]!)
        }
       
    }
    else
    {
        if stotine != 0 {
            if jedinice == 0{
                konacanBroj = String ( brojevi3 [stotine]!) + " " + String ( brojevi3 [desetice]!)
            }else{
                konacanBroj = String ( brojevi3 [stotine]!) + " " + String ( brojevi3 [desetice]!) + " " + String ( brojevi3 [jedinice]!)
            }
            
        }
        else{
            if jedinice == 0{
                konacanBroj = String ( brojevi3 [desetice]!)
            }
            else{
                konacanBroj = String ( brojevi3 [desetice]!) + " " + String (brojevi3 [jedinice]!)
            }
        }
        
    }
    
    //    print(desetice, jedinice)
   
    print("Zadatak broj 3: Tekstualna vrednost broja \(broj), je : \(konacanBroj)")
    break
    
}





