//
//  ViewController.swift
//  Demo05
//
//  Created by milos.drljaca on 4/23/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var NumberOne: UITextField!
    @IBOutlet weak var NumberTwo: UITextField!
    
    @IBAction func Add(_ sender: Any) {
        
        let broj1: Int? = Int(NumberOne.text!)
        let broj2: Int? = Int(NumberTwo.text!)
        
        
        if broj1 != nil && broj2 != nil {
            let c = broj1! + broj2!
            
            Result.text = "Result : \(c)"
        }else{
            Result.text = "No result."
        }
    }
    
    @IBOutlet weak var Result: UILabel!
    
}

