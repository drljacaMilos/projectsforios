//
//  ViewController.swift
//  PalindromeApp
//
//  Created by milos.drljaca on 4/23/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var word: UITextField!
    @IBOutlet weak var result: UITextField!
    
    @IBAction func checkIfPalindrome(_ sender: Any) {
        
        //checking for whitespace in sentence
        let insertedWord = word.text!.filter { !$0.isWhitespace }
        
        
        //Checking if input is empty
        if !insertedWord.isEmpty {
    
        //Creating a palindrome from inserted word
        let palindromeWord = String (insertedWord.reversed())
        
            //Setting words to be uppercased
            if palindromeWord.uppercased()
                == insertedWord.uppercased() {
            
            result.text = "Your word is a palindrome!"
        }
        else
        {
            result.text = "Your word is not a palindrome!"
        }
            
        }
        else
        {
            result.text = "Enter a word!"
        }
        
    }
    
    
    
    
}

