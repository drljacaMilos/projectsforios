//
//  ViewController.swift
//  Demo04
//
//  Created by milos.drljaca on 4/23/21.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var middleName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
    
    
    @IBOutlet weak var result: UILabel!
    
    
    
    @IBAction func submit(_ sender: Any) {
        
        result.text = name.text! + " " + middleName.text! + " " + lastName.text!
    }
    
    
}

